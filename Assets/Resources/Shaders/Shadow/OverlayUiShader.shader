Shader "UI/Default_OverlayNoZTest"
{
    Properties
    {
        [PerRendererData] _$$anonymous$$ainTex("Sprite Texture", 2D) = "white" {}
        _Color("Tint", Color) = (1,1,1,1)

        _StencilComp("Stencil Comparison", Float) = 8
        _Stencil("Stencil ID", Float) = 0
        _StencilOp("Stencil Operation", Float) = 0
        _StencilWrite$$anonymous$$ask("Stencil Write $$anonymous$$ask", Float) = 255
        _StencilRead$$anonymous$$ask("Stencil Read $$anonymous$$ask", Float) = 255

        _Color$$anonymous$$ask("Color $$anonymous$$ask", Float) = 15
    }

        SubShader
        {
            Tags
            {
                "Queue" = "Overlay"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Stencil
            {
                Ref[_Stencil]
                Comp[_StencilComp]
                Pass[_StencilOp]
                Read$$anonymous$$ask[_StencilRead$$anonymous$$ask]
                Write$$anonymous$$ask[_StencilWrite$$anonymous$$ask]
            }

            Cull Off
            Lighting Off
            ZWrite Off
            ZTest Off
            Blend SrcAlpha One$$anonymous$$inusSrcAlpha
            Color$$anonymous$$ask[_Color$$anonymous$$ask]

            Pass
            {
            CGPROGRA$$anonymous$$
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct appdata_t
                {
                    float4 vertex   : POSITION;
                    float4 color    : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                struct v2f
                {
                    float4 vertex   : SV_POSITION;
                    fixed4 color : COLOR;
                    half2 texcoord  : TEXCOORD0;
                };

                fixed4 _Color;
                fixed4 _TextureSampleAdd; //Added for font color support

                v2f vert(appdata_t IN)
                {
                    v2f OUT;
                    OUT.vertex = mul(UNITY_$$anonymous$$ATRIX_$$anonymous$$VP, IN.vertex);
                    OUT.texcoord = IN.texcoord;
    #ifdef UNITY_HALF_TEXEL_OFFSET
                    OUT.vertex.xy += (_ScreenParams.zw - 1.0) * float2(-1,1);
    #endif
                    OUT.color = IN.color * _Color;
                    return OUT;
                }

                sampler2D _$$anonymous$$ainTex;

                fixed4 frag(v2f IN) : SV_Target
                {
                half4 color = (tex2D(_$$anonymous$$ainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;  //Added for font color support
                clip(color.a - 0.01);
                return color;
                }
            ENDCG
            }
        }
}