﻿Shader "Portal/ReadMobileColor" {
	Properties {
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Texture", 2D) = "white" {}
		[IntRange] _StencilRef ("Stencil Reference Value", Range(0,255)) = 0
	}
	SubShader {
		Tags{ "RenderType"="Opaque" "Queue"="Geometry"}

		Stencil{
			Ref [_StencilRef]
			Comp Equal
		}

		Pass {
			Lighting Off

			SetTexture[_MainTex] {
			// Sets our color as the 'constant' variable
			// constantColor[_Color]
			constantColor[_Color]

			// Multiplies color (in constant) with texture
			combine constant * texture
			}
		}
	}

	FallBack "Unlit/Texture"
}