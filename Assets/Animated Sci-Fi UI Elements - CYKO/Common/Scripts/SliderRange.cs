﻿using UnityEngine;
using UnityEngine.UI;

public class SliderRange : MonoBehaviour
{
    [Range(0.1f, 3f)]
    public float speed = 1f;

    public void Update()
    {
        gameObject.GetComponent<Animator>().speed = speed;
    }
}
