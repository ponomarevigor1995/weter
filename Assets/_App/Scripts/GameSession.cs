﻿using System;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;

namespace Weter
{
    public abstract class GameSession : BaseGameSession, IGameSession
    {
        public static GameSession Instance;
        
        [Header("Game Session")]
        
        [SerializeField] protected TouchController m_TowerRoot;
        [SerializeField] protected SessionUI m_SessionUi;

        [SerializeField] protected Tower m_Tower;
        [SerializeField] protected WindController m_Wind;
        [SerializeField] protected TowerFlow m_TowerFlow;

        [SerializeField] public List<GameObject> m_Environments = new List<GameObject>();

        protected GameState m_State = GameState.TUTORIAL;
        protected int m_CurrentEnvironment = 0;

        public GameState State
        {
            get => m_State;
            set => m_State = value;
        }

        protected override void InitComponents()
        {
            base.InitComponents();

            if (!m_TowerRoot)
            {
                m_TowerRoot = FindObjectOfType<TouchController>(true);
            }

            if (!m_SessionUi)
            {
                m_SessionUi = FindObjectOfType<SessionUI>(true);
            }

            if (!m_Tower)
            {
                m_Tower = FindObjectOfType<Tower>(true);
            }

            if (!m_Wind)
            {
                m_Wind = FindObjectOfType<WindController>(true);
            }

            if (!m_TowerFlow)
            {
                m_TowerFlow = FindObjectOfType<TowerFlow>(true);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            Instance = this;
            
            SetTutorialState();

            m_TowerRoot.TurnOn(m_TowerRoot.gameObject.transform,
                               m_CameraTarget,
                               false,
                               false,
                               false,
                               false);
        }

        protected override void Start()
        {
            base.Start();
            m_TowerRoot.OnScale += localScale =>
            {
                m_TowerFlow.UpdateScale(localScale.x);
                UpdateScaleLightObservers(localScale.x);
            };
        }

        protected override void Update()
        {
            base.Update();
            switch (State)
            {
                case GameState.TUTORIAL:
                    break;
                case GameState.PREPARE:
                    UpdatePosition();
                    break;
                case GameState.PLAY:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public virtual void UpdatePosition()
        {
            Vector3? inputPosition = TouchHelper.CheckTouch();
            if (inputPosition != null)
            {
                Ray ray = m_CameraTarget.ScreenPointToRay((Vector3)inputPosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    m_TowerRoot.transform.position = hitInfo.point;
                    Vector3 targetPosition = new Vector3(m_CameraTarget.transform.position.x,
                                                         m_TowerRoot.transform.position.y,
                                                         m_CameraTarget.transform.position.z);
                    m_TowerRoot.transform.LookAt(targetPosition);
                }
            }
        }

        #region Game States

        public virtual void SetTutorialState()
        {
            State = GameState.TUTORIAL;
            m_SessionUi.SetTutorialState();

            m_Tower.EnableShields(m_UseShields);

            m_TowerRoot.TurnOn(
                m_TowerRoot.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                false,
                false);

            if (!Application.isEditor)
            {
                m_TowerRoot.gameObject.SetActive(false);
            }
        }

        public virtual void SetPrepareState()
        {
            State = GameState.PREPARE;

            m_SessionUi.SetPrepareState();

            m_TowerRoot.gameObject.SetActive(true);

            m_TowerRoot.TurnOn(
                m_TowerRoot.gameObject.transform,
                m_CameraTarget,
                true,
                true,
                true,
                true);
        }

        public virtual void SetPlayState()
        {
            State = GameState.PLAY;

            m_TowerRoot.gameObject.SetActive(true);

            m_SessionUi.SetPlayState();

            /*
            m_TowerRoot.TurnOn(
                m_TowerRoot.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                false,
                false);
            */

            m_TowerRoot.TurnOn(
                m_TowerRoot.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                true,
                true);

            m_TowerFlow.EnableShields(m_UseShields);
            m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);

            m_SessionUi.UpdateWind(m_InitWindValue);
            UpdateWindSpeed(m_InitWindValue);
            UpdateScaleLightObservers(m_TowerRoot.transform.localScale.x);
        }

        #endregion

        public override void SwitchShields()
        {
            base.SwitchShields();
        }
        
        public override void EnableShields(bool useShields)
        {
            base.EnableShields(useShields);

            m_TowerFlow.EnableShields(m_UseShields);
            m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);

            m_Tower.EnableShields(m_UseShields);
        }

        public void SwitchEnvironment()
        {
            m_CurrentEnvironment++;
            m_CurrentEnvironment %= m_Environments.Count;

            ShowCity(m_CurrentEnvironment);
        }

        private void ClearCities()
        {
            foreach (GameObject cityController in m_Environments)
            {
                cityController.gameObject.SetActive(false);
            }
        }

        public void ShowCity(int cityPosition)
        {
            ClearCities();
            m_Environments[cityPosition].gameObject.SetActive(true);

            switch (cityPosition)
            {
                case 0:
                    m_Tower.transform.localPosition = new Vector3(0f, 0f, 0f);
                    m_Tower.ShowHex(true);

                    EnableShields(m_UseShields);

                    break;
                case 1:
                    m_Tower.transform.localPosition = new Vector3(0f, 0f, 0f);
                    m_Tower.ShowHex(false);

                    EnableShields(m_UseShields);
                    UpdateScaleLightObservers(m_TowerRoot.transform.localScale.x);

                    break;
                case 2:

                    m_Tower.transform.localPosition = new Vector3(0f, 0.62f, 0f);
                    m_Tower.ShowHex(false);

                    EnableShields(m_UseShields);
                    UpdateScaleLightObservers(m_TowerRoot.transform.localScale.x);

                    break;
            }
        }

        #region Settings TouchRoot

        public void Rotate(float rotation)
        {
            m_TowerRoot.transform.localRotation = Quaternion.Euler(0f, rotation, 0f);
        }

        public void Scale(float scale)
        {
            m_TowerRoot.transform.localScale = new Vector3(scale, scale, scale);

            m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);
            UpdateScaleLightObservers(m_TowerRoot.transform.localScale.x);
        }

        #endregion
    }
}