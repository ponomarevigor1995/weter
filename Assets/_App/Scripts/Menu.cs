using System.Collections;
using System.Collections.Generic;
using _App.Scripts.Karandash_SpatialMap.SpatialMap;
using easyar;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class Menu : MonoBehaviour
    {
        public ARSession ARSession;
        public LocalRepository LocalRepository;
        public SceneManager SceneManager;
        
        public GameObject ErrorPanel;

        public Button ClientScanButton;
        public Button AdminScanButton;

        public Button SlamButton;
        public Button SurfaceButton;

        private void Start()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                ARSession.StateChanged += state =>
                {
                    if (state == ARSession.SessionState.Ready)
                    {
                        if (ARCoreCameraDevice.isAvailable())
                        {
                            ErrorPanel.gameObject.SetActive(false);

                            ClientScanButton.interactable = true;
                            AdminScanButton.interactable = true;

                            //SlamButton.gameObject.SetActive(true);
                            SlamButton.interactable = true;

                            //SlamButton.gameObject.SetActive(false);
                            //SurfaceButton.interactable = false;
                            SurfaceButton.interactable = true;
                        }
                        else
                        {
                            ErrorPanel.gameObject.SetActive(true);

                            ClientScanButton.interactable = false;
                            AdminScanButton.interactable = false;

                            //SlamButton.gameObject.SetActive(false);
                            SlamButton.interactable = false;

                            //SlamButton.gameObject.SetActive(true);
                            SurfaceButton.interactable = true;
                        }
                    }
                };
            }
        }

        public void OpenAdminMode()
        {
            LocalRepository.SetAdminMode();
            SceneManager.LoadScene("SpatialMap");
        }

        public void OpenClientMode()
        {
            LocalRepository.SetClientMode();
            SceneManager.LoadScene("SpatialMap");
        }
    }
}