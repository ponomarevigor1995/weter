using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;

namespace Weter
{
    public class HouseController : LightObserver
    {
        [SerializeField] private GameObject m_LightWindow;
        
        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void UpdateLight(float light)
        {
            base.UpdateLight(light);
            if (m_Light <= m_Night)
            {
                m_LightWindow.SetActive(true);
            }
            else
            {
                m_LightWindow.SetActive(false);
            }
        }

        public override void UpdateLightScale(float scale)
        {
            base.UpdateLightScale(scale);
        }
    }
}