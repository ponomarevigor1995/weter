using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;

namespace Weter
{
    public class LampController : LightObserver
    {
        [SerializeField] private Light m_Lamp;
        
        private float m_Range = 1.6f;
        private float m_Intensity = 1.5f;
            
        protected override void Awake()
        {
            base.Awake();
            if (m_Lamp == null)
            {
                m_Lamp = GetComponentInChildren<Light>(true);
            }
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void UpdateLight(float light)
        {
            base.UpdateLight(light);
            if (!enabled)
            {
                return;
            }
            if (m_Light <= m_Night)
            {
                m_Lamp.gameObject.SetActive(true);
            }
            else
            {
                m_Lamp.gameObject.SetActive(false);
            }
        }

        public override void UpdateLightScale(float scale)
        {
            base.UpdateLightScale(scale);
            if (m_Lamp != null)
            {
                m_Lamp.range = m_Range * scale;
                m_Lamp.intensity = m_Intensity;
            }
        }
    }
}