namespace Weter
{
    public interface ILightObserver
    {
        public void UpdateLight(float light);
        
        public void UpdateLightScale(float scale);
    }
}