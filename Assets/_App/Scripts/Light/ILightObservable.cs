﻿namespace Weter
{
    public interface ILightObservable
    {
        public void AddLightObserver(ILightObserver observer);
        
        public void RemoveLightObserver(ILightObserver observer);
        
        public void UpdateLightObservers(float light);

        public void UpdateScaleLightObservers(float scale);

        //public void UpdateLightObservers(float light, float Scale);
    }
}