using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using Weter;

namespace Weter
{
    public class Sun : LightObserver
    {
        [SerializeField] private Light m_DirectionalLight;
        [SerializeField] private float m_MinAngle = -45f;
        [SerializeField] private float m_MaxAngle = 0f;
        [SerializeField] private float m_CurrentAngle = 0f;
        
        [SerializeField] private float m_MinIntensity = 0.4f;
        [SerializeField] private float m_MaxIntensity = 1.2f;
        [SerializeField] private float m_CurrentIntensity = 1.0f;

        private float m_AngleRange = 0f;

        protected override void Awake()
        {
            base.Awake();
            m_AngleRange = Mathf.Abs(m_MaxAngle - m_MinAngle);
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void UpdateLight(float light)
        {
            base.UpdateLight(light);
            m_CurrentAngle = (1 - m_Light) * m_AngleRange * (-1);
            transform.localRotation = Quaternion.Euler(m_CurrentAngle, m_CurrentAngle/2f, m_CurrentAngle/4f);

            m_CurrentIntensity = m_Light + 0.1f;
            /*
            if (m_Light < m_MinIntensity)
            {
                m_CurrentIntensity = m_MinIntensity;
            }

            if (m_Light > m_MaxIntensity)
            {
                m_CurrentIntensity = m_MaxIntensity;
            }
            */
            
            m_DirectionalLight.intensity = m_CurrentIntensity;
            m_DirectionalLight.shadowStrength = m_Light * 0.8f;
        }
        
        public override void UpdateLightScale(float scale)
        {
            base.UpdateLightScale(scale);
        }
    }
}