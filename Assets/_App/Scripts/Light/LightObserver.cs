using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;
using Weter.Slam;

namespace Weter
{
    public class LightObserver : ExtendedMonoBehaviour, ILightObserver
    {
        [SerializeField] private GameObject m_LightGameObject;

        [SerializeField] protected float m_Light = 0.0f;
        [SerializeField] protected float m_Night = 0.6f;
        [SerializeField] protected float m_Scale = 1f;

        protected ILightObservable m_LightObserver;

        protected override void InitComponents()
        {
            base.InitComponents();
            
            if (!m_LightGameObject)
            {
                //m_LightObserver = m_LightGameObject.GetComponent<ILightObservable>();
                m_LightGameObject = GameObject.Find("WorldController");
                if (!m_LightGameObject)
                {
                    m_LightGameObject = GameObject.Find("GameSession");
                    //m_LightGameObject = FindObjectOfType<GameSession>().gameObject;
                }
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            ValidateHelper.Validate<ILightObserver>(m_LightGameObject);
        }

        protected virtual void Awake()
        {
            InitComponents();
      
            if (m_LightGameObject != null)
            {
                m_LightObserver = m_LightGameObject.GetComponent<ILightObservable>();
                m_LightObserver.AddLightObserver(this);
            }
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }

        public virtual void UpdateLight(float light)
        {
            m_Light = light;
            if (!enabled)
            {
                return;
            }
        }

        public virtual void UpdateLightScale(float scale)
        {
            m_Scale = scale;
        }
    }
}