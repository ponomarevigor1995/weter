﻿namespace Weter
{
    public enum GameState
    {
        TUTORIAL,
        PREPARE,
        PLAY
    }

    public interface IGameSession
    {
        public void SetTutorialState();
        public void SetPrepareState();
        public void SetPlayState();
    }
}