using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;
using Firebase.Extensions;
using Weter;
using ZBoom.Common.SpatialMap;

namespace Weter.Slam
{
    public class SpatialMapFirebaseController : MonoBehaviour
    {
        private const string MESSAGE_ERROR = "ERROR";
        private const string MESSAGE_SUCCESS = "SUCCESS";

        private string m_SpatialMapsReferenceName = "spatial_maps";
        private DatabaseReference m_DatabaseReference;

        private void Awake()
        {
            m_DatabaseReference = FirebaseDatabase.DefaultInstance.GetReference(m_SpatialMapsReferenceName);
        }

        public void GetSpatialMap(IResultListener<List<SpatialMapData>> resultListener)
        {
            m_DatabaseReference
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        resultListener.OnError(MESSAGE_ERROR);
                    }
                    else
                    {
                        if (task.IsCompleted)
                        {
                            List<SpatialMapData> mapContainers = new List<SpatialMapData>();
                            DataSnapshot snapshot = task.Result;
                            if (snapshot.ChildrenCount > 0)
                            {
                                var childs = snapshot.Children.GetEnumerator();
                                while (childs.MoveNext())
                                {
                                    string json = childs.Current.GetRawJsonValue();
                                    SpatialMapData spatialMapData =
                                        JsonUtility.FromJson<SpatialMapData>(json);

                                    mapContainers.Add(spatialMapData);
                                }

                                resultListener.OnSuccess(mapContainers, MESSAGE_SUCCESS);
                            }
                            else
                            {
                                resultListener.OnSuccess(mapContainers, MESSAGE_ERROR);
                            }
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                });
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData,
            IResultListener<SpatialMapData> resultListener)
        {
            m_DatabaseReference
                .Child(spatialMapData.id)
                .SetRawJsonValueAsync(JsonUtility.ToJson(spatialMapData))
                .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }

                        if (task.IsCompleted)
                        {
                            resultListener.OnSuccess(spatialMapData, MESSAGE_SUCCESS);
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                );
        }
        
        public void RemoveSpatialMap(string idSpatialMap,
            IResultListener<SpatialMapData> resultListener)
        {
            m_DatabaseReference
                .Child(idSpatialMap)
                .RemoveValueAsync()
                .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }

                        if (task.IsCompleted)
                        {
                            resultListener.OnSuccess(null, MESSAGE_SUCCESS);
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                );
        }


        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}

public class IResultListener<T>
{
    public Action<T, string> OnSuccess;

    public Action<string> OnError;

    public Action OnFinish;
}

[Serializable]
public class SpatialMapContainer
{
    public List<SpatialMapData> spatialMaps = new List<SpatialMapData>();

    public SpatialMapContainer()
    {
    }
}

[Serializable]
public class SpatialMapData
{
    public string id = "";
    public string name = "";
    public List<SpatialMapItemData> mapItems = new List<SpatialMapItemData>();

    public SpatialMapData()
    {
    }
}

/*[Serializable]
public class SpatialMapItemData
{
    public string Id = "";
    public string Name = "";

    public float[] Position = new float[3];
    public float[] Rotation = new float[4];
    public float[] EulerRotation = new float[3];
    public float[] Scale = new float[3];

    public float positionX = 0f;
    public float positionY = 0f;
    public float positionZ = 0f;

    public float rotationX = 0f;
    public float rotationY = 0f;
    public float rotationZ = 0f;

    public float scaleX = 0f;
    public float scaleY = 0f;
    public float scaleZ = 0f;

    //TowerGameObject Settings
    public int EnabledUI = (int) TowerSettings.UiType.ENABLE;
    public int EnabledTower = (int) TowerSettings.TowerType.ENABLE;
    public int EnabledHex = (int) TowerSettings.HexType.ENABLE;
    public int EnabledShield = (int) TowerSettings.ShieldType.ENABLE;
    public int EnabledFlow = (int) TowerSettings.FlowType.ENABLE;
    
    public SpatialMapItemData()
    {
    }
}*/