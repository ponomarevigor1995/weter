﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class MainViewController : MonoBehaviour
    {
        public Button EditButton;
        public Button PreviewButton;
        public Button CreateButton;
        public SpatialMapGridController SpatialMapGridController;

        public GameObject ProgressPanel;
        public GameObject ErrorPanel;

        private void OnEnable()
        {
            StopAllCoroutines();
        }

        public void EnableEdit(bool enable)
        {
            EditButton.interactable = enable;
        }

        public void EnablePreview(bool enable)
        {
            PreviewButton.interactable = enable;
        }

        public void SetData(List<MapMeta> mapMetas)
        {
            SpatialMapGridController.SetData(mapMetas);
        }

        public void ShowError(bool isShow)
        {
            if (isShow)
            {
                ErrorPanel.SetActive(true);                
            }
            else
            {
                ErrorPanel.SetActive(false);                
            }
        }
        
        public void ShowLoading(bool isShow)
        {
            if (isShow)
            {
                ProgressPanel.SetActive(true);                
            }
            else
            {
                ProgressPanel.SetActive(false);                
            }
        }
    }
}
