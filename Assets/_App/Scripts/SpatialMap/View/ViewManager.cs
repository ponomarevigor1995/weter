﻿//================================================================================================================================
//
//  Copyright (c) 2015-2021 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

using easyar;
using System;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;
using Weter;
using Weter.Slam;
using ZBoom.Common.SpatialMap;
using Object = UnityEngine.Object;

namespace Weter
{
    public class ViewManager : MonoBehaviour
    {
        public static ViewManager Instance;

        public SpatialMapGameSession GameSession;
        
        public GameObject EasyARSession;
        public SpatialMapFirebaseController FirebaseController;
        public SparseSpatialMapController MapControllerPrefab;
        public MainViewController MainView;
        public CreateViewController CreateView;
        public EditViewController EditView;
        public GameObject PreviewView;
        public Text Status;
        public SwitchManager CloudPointSwitch;
        public bool MainViewRecycleBinClearMapCacheOnly;

        public FloorController FloorController;

        public bool ShowStatus = false;

        private GameObject m_EasyarObject;
        private ARSession m_Session;
        private VIOCameraDeviceUnion m_VioCamera;
        private SparseSpatialMapWorkerFrameFilter m_MapFrameFilter;
        private List<MapMeta> m_SelectedMaps = new List<MapMeta>();
        private MapSession m_MapSession;
        private string m_DeviceModel = string.Empty;


#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
        static void ImportSampleStreamingAssets()
        {
            FileUtil.ImportSampleStreamingAssets();
        }
#endif

        private void Awake()
        {
            Instance = this;
            MainView.gameObject.SetActive(false);
            CreateView.gameObject.SetActive(false);
            EditView.gameObject.SetActive(false);
            PreviewView.SetActive(false);
        }

        private void Start()
        {
            LoadMainView();
        }

        private void Update()
        {
            if (m_Session)
            {
                if (ShowStatus)
                {
                    Status.text = $"Device Model: {SystemInfo.deviceModel} {m_DeviceModel}" + Environment.NewLine +
                                  "VIO Device" + Environment.NewLine +
                                  "\tType: " +
                                  ((m_Session.Assembly != null && m_Session.Assembly.FrameSource)
                                      ? m_Session.Assembly.FrameSource.GetType().ToString().Replace("easyar.", "")
                                          .Replace("FrameSource", "")
                                      : "-") + Environment.NewLine +
                                  "\tTracking Status: " + m_Session.TrackingStatus + Environment.NewLine +
                                  "CenterMode: " + m_Session.CenterMode + Environment.NewLine +
                                  "Sparse Spatial Map" + Environment.NewLine +
                                  "\tWorking Mode: " + m_MapFrameFilter.WorkingMode + Environment.NewLine +
                                  "\tLocalization Mode: " + m_MapFrameFilter.LocalizerConfig.LocalizationMode +
                                  Environment.NewLine +
                                  "Localized Map" + Environment.NewLine +
                                  "\tName: " +
                                  (m_MapFrameFilter.LocalizedMap == null
                                      ? "-"
                                      : (m_MapFrameFilter.LocalizedMap.MapInfo == null
                                          ? "-"
                                          : m_MapFrameFilter.LocalizedMap.MapInfo.Name)) + Environment.NewLine +
                                  "\tID: " + (m_MapFrameFilter.LocalizedMap == null
                                      ? "-"
                                      : (m_MapFrameFilter.LocalizedMap.MapInfo == null
                                          ? "-"
                                          : m_MapFrameFilter.LocalizedMap.MapInfo.ID)) + Environment.NewLine +
                                  "\tPoint Cloud Count: " + (m_MapFrameFilter.LocalizedMap == null
                                      ? "-"
                                      : m_MapFrameFilter.LocalizedMap.PointCloud.Count.ToString());
                }

                if (m_MapFrameFilter.LocalizedMap == null)
                {
                    CloudPointSwitch.gameObject.SetActive(false);
                }
                else
                {
                    CloudPointSwitch.gameObject.SetActive(true);
                }
            }
            else
            {
                Status.text = string.Empty;
            }
        }

        private void OnDestroy()
        {
            DestroySession();
        }

        public void SelectMaps(List<MapMeta> metas)
        {
            m_SelectedMaps = metas;
            MainView.EnablePreview(m_SelectedMaps.Count > 0);
            MainView.EnableEdit(m_SelectedMaps.Count == 1);
        }

        public void LoadMainView(bool isLoadData = true)
        {
            DestroySession();
            SelectMaps(new List<MapMeta>());
            MainView.gameObject.SetActive(true);
            if (isLoadData)
            {
                GetSpatialMaps();
            }
        }

        public void LoadCreateView()
        {
            CreateSession();
            m_MapSession.SetupMapBuilder(MapControllerPrefab);
            CreateView.SetMapSession(m_MapSession);
            CreateView.gameObject.SetActive(true);
        }

        public void LoadEditView()
        {
            CreateSession();
            //m_MapSession.LoadMapMeta(SparseSpatialMapPrefab, true);
            m_MapSession.LoadMapMeta(
                MapControllerPrefab, 
                true, 
                objectCreatedEvent:ModifyObject);
            
            EditView.SetMapSession(m_MapSession);
            EditView.gameObject.SetActive(true);
            CloudPointSwitch.isOn = true;
        }

        public void LoadEditView(MapSession mapSession, string id)
        {
            if (mapSession != null)
            {
                m_MapSession = mapSession;
                CreateView.gameObject.SetActive(false);
                Debug.Log("ViewManager: 1");
                Debug.Log("ViewManager: 2");

                var mapData = new MapSession.MapData();
                var mapMeta = new MapMeta(m_MapSession.MapWorker.BuilderMapController.MapInfo,
                    new List<MapMeta.PropInfo>());
                //Debug.Log("ViewManager: 2 1 " + m_MapSession.MapWorker.BuilderMapController.MapInfo.Name); //NULL
                var mapMeta1 = MapMetaManager.LoadAll().Find(item => item.Map.ID.Equals(id));

                Debug.Log("ViewManager: 2 2 " + mapMeta1.Map.Name);
                //mapData.Meta = mapMeta;
                //mapData.Controller = SparseSpatialMapPrefab;
                //mapData.Props = new List<GameObject>();
                //m_MapSession.Maps.Add(mapData);

                m_MapSession.Maps = new List<MapSession.MapData>();
                //m_MapSession.Maps[0] = new MapSession.MapData() { Meta = mapMeta };
                m_MapSession.Maps.Add(new MapSession.MapData() {Meta = mapMeta1});

                //m_MapSession.LoadMapMeta(m_MapSession.MapWorker.BuilderMapController);
                m_MapSession.LoadMapMeta(MapControllerPrefab, true);
                Debug.Log("ViewManager: 2 5");

                EditView.SetMapSession(m_MapSession);
                Debug.Log("ViewManager: 3");
                EditView.gameObject.SetActive(true);
                CloudPointSwitch.isOn = true;
            }
            else
            {
                LoadEditView();
            }
        }

        public void LoadPreviewView()
        {
            GameSession.Clear();
            
            CreateSession();
            m_MapSession.LoadMapMeta(
                MapControllerPrefab, 
                false, 
                objectCreatedEvent:ModifyObject, 
                //mapLoadEvent:PreviewMapLoad,
                mapLocalized:() => GameSession.Activate()
                );
            PreviewView.SetActive(true);
        }

        private void ModifyObject(GameObject prop, MapMeta.PropInfo propInfo)
        {
            TowerSettings towerSettingsController = prop.GetComponent<TowerSettings>();
            if (towerSettingsController != null)
            {
                Debug.LogError("PropInfo: " + 1000);

                SpatialMapItemData spatialMapItemData = new SpatialMapItemData()
                {
                    Id = propInfo.Name,
                    Name = propInfo.Name,
                    Position = new float[3]
                    {
                        prop.transform.localPosition.x, prop.transform.localPosition.y,
                        prop.transform.localPosition.z
                    },
                    Rotation = new float[4]
                    {
                        prop.transform.localRotation.x, prop.transform.localRotation.y,
                        prop.transform.localRotation.z, prop.transform.localRotation.w
                    },
                    EulerRotation = new float[3]
                    {
                        prop.transform.localRotation.eulerAngles.x, prop.transform.localRotation.eulerAngles.y,
                        prop.transform.localRotation.eulerAngles.z
                    },
                    Scale = new float[3]
                        {prop.transform.localScale.x, prop.transform.localScale.y, prop.transform.localScale.z},

                    EnabledFlow = propInfo.EnableFlow,
                    EnabledHex = propInfo.EnableHex,
                    EnabledShield = propInfo.EnableShield,
                    EnabledTower = propInfo.EnableTower,
                    EnabledUI = propInfo.EnableUI
                };


                towerSettingsController.TowerData = spatialMapItemData;
                //towerSettingsController.SetData(spatialMapItemData);
            }
        }

        private void PreviewMapLoad(SparseSpatialMapController.SparseSpatialMapInfo map)
        {
            GameSession.Activate();
        }

        public void ShowParticle(bool show)
        {
            if (m_MapSession == null)
            {
                return;
            }

            foreach (var map in m_MapSession.Maps)
            {
                if (map.Controller)
                {
                    map.Controller.ShowPointCloud = show;
                }
            }
        }

        private void CreateSession()
        {
            m_EasyarObject = Instantiate(EasyARSession);
            m_EasyarObject.SetActive(true);
            m_Session = m_EasyarObject.GetComponent<ARSession>();
            m_VioCamera = m_EasyarObject.GetComponentInChildren<VIOCameraDeviceUnion>();
            m_MapFrameFilter = m_EasyarObject.GetComponentInChildren<SparseSpatialMapWorkerFrameFilter>();

            //TODO: INIT MAP SESSION

            //m_MapSession = new MapSession(m_MapFrameFilter, m_SelectedMaps);
            m_MapSession = new MapSession(m_Session, m_MapFrameFilter, m_SelectedMaps);

            if (FloorController != null)
            {
                FloorController.Reset(m_VioCamera);
            }
        }

        private void DestroySession()
        {
            if (m_MapSession != null)
            {
                m_MapSession.Dispose();
                m_MapSession = null;
            }

            if (m_EasyarObject)
            {
                Destroy(m_EasyarObject);
            }
        }


        public void GetSpatialMaps()
        {
            MainView.ShowError(false);
            MainView.ShowLoading(true);

            FirebaseController.GetSpatialMap(new IResultListener<List<SpatialMapData>>()
            {
                OnError = errorMessage =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        MainView.ShowLoading(false);
                        MainView.ShowError(true);
                    });
                },
                OnSuccess = (spatialMaps, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        List<MapMeta> mapMetas = new List<MapMeta>();
                        foreach (var spatialMapData in spatialMaps)
                        {
                            SparseSpatialMapController.SparseSpatialMapInfo spatialMapInfo =
                                new SparseSpatialMapController.SparseSpatialMapInfo();
                            spatialMapInfo.Name = spatialMapData.name;
                            spatialMapInfo.ID = spatialMapData.id;

                            List<MapMeta.PropInfo> props = new List<MapMeta.PropInfo>();

                            foreach (var spatialMapItemData in spatialMapData.mapItems)
                            {
                                MapMeta.PropInfo propInfo = new MapMeta.PropInfo();
                                propInfo.Name = spatialMapItemData.Id;

                                propInfo.Position = spatialMapItemData.Position;
                                propInfo.Rotation = spatialMapItemData.Rotation;
                                propInfo.Scale = spatialMapItemData.Scale;

                                propInfo.EnableFlow = spatialMapItemData.EnabledFlow;
                                propInfo.EnableHex = spatialMapItemData.EnabledHex;
                                propInfo.EnableShield = spatialMapItemData.EnabledShield;
                                propInfo.EnableTower = spatialMapItemData.EnabledTower;
                                propInfo.EnableUI = spatialMapItemData.EnabledUI;

                                /*
                                propInfo.Position = new float[3]
                                {
                                    spatialMapItemData.positionX,
                                    spatialMapItemData.positionY,
                                    spatialMapItemData.positionZ
                                };

                                propInfo.Rotation = new float[3]
                                {
                                    spatialMapItemData.rotationX,
                                    spatialMapItemData.rotationY,
                                    spatialMapItemData.rotationZ
                                };

                                propInfo.Scale = new float[3]
                                {
                                    spatialMapItemData.scaleX,
                                    spatialMapItemData.scaleY,
                                    spatialMapItemData.scaleZ
                                };
                                */


                                props.Add(propInfo);
                            }

                            MapMeta mapMeta = new MapMeta(spatialMapInfo, props);
                            mapMetas.Add(mapMeta);
                        }

                        MainView.SetData(mapMetas);

                        MainView.ShowLoading(false);
                        MainView.ShowError(false);
                    });
                }
            });
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData)
        {
            FirebaseController.SaveSpatialMap(spatialMapData, new IResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) => { UnityMainThreadDispatcher.Instance().Enqueue(() => { }); },
                OnError = (errorMessage) => { UnityMainThreadDispatcher.Instance().Enqueue(() => { }); }
            });
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData, IResultListener<SpatialMapData> resultListener)
        {
            FirebaseController.SaveSpatialMap(spatialMapData, resultListener);
        }

        public void RemoveSpatialMap(string idSpatialMap, IResultListener<SpatialMapData> resultListener)
        {
            FirebaseController.RemoveSpatialMap(idSpatialMap, resultListener);
        }
    }
}