﻿using easyar;
using System;
using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class CreateViewController : MonoBehaviour
    {
        public GameObject UploadPopup;
        public GameObject ProgressBar;
        public TMP_InputField MapNameInput;
        public Button UploadEasyArButton;
        public Button UploadFirebaseButton;
        public Button UploadCancelButton;
        public Button SaveButton;
        public Button SnapshotButton;
        public RawImage PreviewImage;

        private MapSession m_MapSession;
        private string m_MapName;
        private string m_MapId = "";
        private Texture2D m_CapturedImage;

        private void OnEnable()
        {
            SaveButton.gameObject.SetActive(true);
            SaveButton.interactable = false;

            StopUploadUI();
            UploadPopup.gameObject.SetActive(false);
            
            var buttonEasyArText = UploadEasyArButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonEasyArText.text = "Сохранить";
            
            var buttonFirebaseText = UploadFirebaseButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonFirebaseText.text = "Сохранить";
        }

        private void Update()
        {
            if ((m_MapSession.MapWorker.LocalizedMap == null ||
                 m_MapSession.MapWorker.LocalizedMap.PointCloud.Count <= 20) && !Application.isEditor)
            {
                SaveButton.interactable = false;
            }
            else
            {
                SaveButton.interactable = true;
            }
        }

        private void OnDestroy()
        {
            if (m_CapturedImage)
            {
                Destroy(m_CapturedImage);
            }
        }

        public void SetMapSession(MapSession session)
        {
            m_MapSession = session;
        }

        public void Save()
        {
            SaveButton.gameObject.SetActive(false);
            UploadPopup.gameObject.SetActive(true);
            //MapNameInput.text = m_MapName = "Map_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm");
            MapNameInput.text = m_MapName = "Карта: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            m_MapSession.MapWorker.enabled = false;
            Snapshot();
        }

        public void Snapshot()
        {
            var oneShot = Camera.main.gameObject.AddComponent<OneShot>();
            oneShot.Shot(true, (texture) =>
            {
                if (m_CapturedImage)
                {
                    Destroy(m_CapturedImage);
                }

                m_CapturedImage = texture;
                PreviewImage.texture = m_CapturedImage;
            });
        }

        public void OnMapNameChange(string name)
        {
            UploadEasyArButton.interactable = !string.IsNullOrEmpty(name);
            m_MapName = name;
        }

        public void Upload()
        {
            UploadEasyArButton.gameObject.SetActive(true);
            UploadFirebaseButton.gameObject.SetActive(false);
            
            using (var buffer = easyar.Buffer.wrapByteArray(m_CapturedImage.GetRawTextureData()))
            using (var image =
                new easyar.Image(buffer, PixelFormat.RGB888, m_CapturedImage.width, m_CapturedImage.height))
            {
                m_MapSession.MapWorker.MapUnload += (controller, info, arg3, arg4) =>
                {
                    Debug.Log("ViewManager: 0 ");
                    if (info != null)
                    {
                        Debug.Log("ViewManager: 0 " + info.Name + " / " + info.ID);
                    }
                };

                m_MapSession.MapWorker.BuilderMapController.MapHost += (map, isSuccessful, error) =>
                {
                    if (isSuccessful)
                    {
                        Debug.Log("ViewManager: -1 ");
                        Debug.Log("ViewManager: -1 " + map.Name + " / " + map.ID);
                        m_MapId = map.ID;
                    }
                    else
                    {
                    }
                };

                m_MapSession.Save(m_MapName, image);
            }

            StartUploadUI();
            StartCoroutine(Saving());
        }

        private IEnumerator Saving()
        {
            while (m_MapSession.IsSaving)
            {
                yield return 0;
            }

            if (m_MapSession.Saved)
            {
                //TODO

                SaveFirebase();

                //Debug.Log("ViewManager: 1 " + m_MapSession.MapWorker.BuilderMapController.);
                //ViewManager.Instance.LoadEditView(m_MapSession, m_MapSession.MapWorker.BuilderMapController.MapInfoSource.ID);

                //ViewManager.Instance.LoadEditView(m_MapSession, m_MapId);
            }
            else
            {
                StopUploadUI();
                ShowErrorEasyAR();
            }
            //StopUploadUI();
        }

        public void SaveFirebase()
        {
            SpatialMapData spatialMapData = new SpatialMapData();
            spatialMapData.id = m_MapId;
            spatialMapData.name = m_MapName;

            ViewManager.Instance.SaveSpatialMap(spatialMapData, new IResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        StopUploadUI();
                        gameObject.SetActive(false);
                        ViewManager.Instance.LoadMainView();
                    });
                },

                OnError = (errorMessage) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        StopUploadUI();
                        ShowErrorFirebase();
                    });
                }
            });
        }

        private void ShowErrorEasyAR()
        {
            UploadEasyArButton.gameObject.SetActive(true);
            UploadFirebaseButton.gameObject.SetActive(false);
            
            var buttonText = UploadEasyArButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = "Повторить";
        }
        
        private void ShowErrorFirebase()
        {
            UploadEasyArButton.gameObject.SetActive(false);
            UploadFirebaseButton.gameObject.SetActive(true);
            
            var buttonText = UploadFirebaseButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = "Повторить";
        }

        private void StartUploadUI()
        {
            UploadEasyArButton.interactable = false;
            UploadFirebaseButton.interactable = false;
            MapNameInput.interactable = false;
            UploadCancelButton.interactable = false;
            SnapshotButton.interactable = false;
            ProgressBar.SetActive(true);
        }

        private void StopUploadUI()
        {
            UploadEasyArButton.interactable = true;
            UploadFirebaseButton.interactable = true;
            MapNameInput.interactable = true;
            UploadCancelButton.interactable = true;
            SnapshotButton.interactable = true;
            ProgressBar.SetActive(false);
        }
    }
}