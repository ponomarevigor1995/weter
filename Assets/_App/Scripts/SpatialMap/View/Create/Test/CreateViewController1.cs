﻿using easyar;
using System;
using System.Collections;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class CreateViewController1 : MonoBehaviour
    {
        public UploadController UploadController;
        public Button SaveButton;
        
        private MapSession m_MapSession;
        private Texture2D m_CapturedImage;
        
        private string m_DefaultTitle = "Создание Spatial Map";

        private void OnEnable()
        {
            SaveButton.gameObject.SetActive(true);
            SaveButton.interactable = false;

            StopUploadUI();
        }

        private void Update()
        {
            if ((m_MapSession.MapWorker.LocalizedMap == null || m_MapSession.MapWorker.LocalizedMap.PointCloud.Count <= 20) && !Application.isEditor)
            {
                SaveButton.interactable = false;
            }
            else
            {
                SaveButton.interactable = true;
            }
        }

        private void OnDestroy()
        {
            if (m_CapturedImage)
            {
                Destroy(m_CapturedImage);
            }
        }

        public void SetMapSession(MapSession session)
        {
            m_MapSession = session;
        }

        public void Save()
        {
            SaveButton.gameObject.SetActive(false);
            UploadController.Show();
            UploadController.MapName = "Map_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm");
            m_MapSession.MapWorker.enabled = false;
            Snapshot();
        }

        public void Snapshot()
        {
            var oneShot = Camera.main.gameObject.AddComponent<OneShot>();
            oneShot.Shot(true, (texture) =>
            {
                if (m_CapturedImage)
                {
                    Destroy(m_CapturedImage);
                }
                m_CapturedImage = texture;
                UploadController.SetPreview(m_CapturedImage);
            });
        }

        public void Upload()
        {
            using (var buffer = easyar.Buffer.wrapByteArray(m_CapturedImage.GetRawTextureData()))
            using (var image = new easyar.Image(buffer, PixelFormat.RGB888, m_CapturedImage.width, m_CapturedImage.height))
            {
                m_MapSession.Save(UploadController.MapName, image);
            }
            StartUploadUI();
            StartCoroutine(SavingStatus());
            StartCoroutine(Saving());
        }

        private IEnumerator Saving()
        {
            while (m_MapSession.IsSaving)
            {
                yield return 0;
            }
            if (m_MapSession.Saved)
            {
                gameObject.SetActive(false);
                ViewManager.Instance.LoadMainView();
            }
            else
            {
                UploadController.SetError();
                StopUploadUI();
            }
        }

        private IEnumerator SavingStatus()
        {
            while (m_MapSession.IsSaving)
            {
                yield return new WaitForSeconds(1);
            }
        }

        private void StartUploadUI()
        {
            UploadController.StartUpload();
        }

        private void StopUploadUI()
        {
            UploadController.StopUpload();
        }
    }
}
