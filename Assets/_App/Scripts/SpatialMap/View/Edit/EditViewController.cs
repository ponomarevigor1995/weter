﻿//================================================================================================================================
//
//  Copyright (c) 2015-2021 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

using Michsky.UI.ModernUIPack;
using System.Collections.Generic;

using UnityEngine;
using Weter;
using ZBoom.Common.SpatialMap;

namespace Weter
{
    public class EditViewController : MonoBehaviour
    {
        public SpatialMapGameObjectController SpatialMapGameObjectController;
        public GameObject ProgressPanel;
        public GameObject ErrorPanel;

        private MapSession.MapData m_MapData;
        private bool m_IsTipsOn;

        private void Awake()
        {
            SpatialMapGameObjectController.CreateObjectEvent += (gameObj) =>
            {
                if (gameObj)
                {
                    gameObj.transform.parent = m_MapData.Controller.transform;
                    m_MapData.Props.Add(gameObj);
                }
            };
            SpatialMapGameObjectController.DeleteObjectEvent += (gameObj) =>
            {
                if (gameObj)
                {
                    m_MapData.Props.Remove(gameObj);
                }
            };
        }

        private void OnEnable()
        {
            m_IsTipsOn = false;
        }

        public void SetMapSession(MapSession session)
        {
            Debug.Log("ViewManager: 3 1");
            m_MapData = session.Maps[0];
            Debug.Log("ViewManager: 3 2");
            SpatialMapGameObjectController.SetMapSession(session);
            Debug.Log("ViewManager: 3 3");
        }

        public void Save(bool isSaveToMetaManager = false)
        {
            if (m_MapData == null)
            {
                return;
            }

            ShowLoading(true);
            ShowError(false);

            List<MapMeta.PropInfo> propInfos = new List<MapMeta.PropInfo>();
            SpatialMapData spatialMapData = new SpatialMapData()
            {
                id = m_MapData.Meta.Map.ID,
                name = m_MapData.Meta.Map.Name
            };

            foreach (var prop in m_MapData.Props)
            {
                var propName = prop.name;
                var position = prop.transform.localPosition;
                var rotation = prop.transform.localRotation;
                var eulerRotation = prop.transform.localRotation.eulerAngles;
                var scale = prop.transform.localScale;

                propInfos.Add(new MapMeta.PropInfo()
                {
                    Name = propName,
                    Position = new float[3] {position.x, position.y, position.z},
                    Rotation = new float[4] {rotation.x, rotation.y, rotation.z, rotation.w},
                    Scale = new float[3] {scale.x, scale.y, scale.z}
                });

                SpatialMapItemData spatialMapItemData = new SpatialMapItemData()
                {
                    Id = propName,
                    Name = propName,
                    Position = new float[3] {position.x, position.y, position.z},
                    Rotation = new float[4] {rotation.x, rotation.y, rotation.z, rotation.w},
                    EulerRotation = new float[3] {eulerRotation.x, eulerRotation.y, eulerRotation.z},
                    Scale = new float[3] {scale.x, scale.y, scale.z}
                };

                TowerSettings towerSettingsController =
                    prop.transform.GetComponent<TowerSettings>();
                if (towerSettingsController != null)
                {
                    spatialMapItemData.EnabledFlow = (int) towerSettingsController.CurrentFlowType;
                    spatialMapItemData.EnabledHex = (int) towerSettingsController.CurrentHexType;
                    spatialMapItemData.EnabledShield = (int) towerSettingsController.CurrentShieldType;
                    spatialMapItemData.EnabledTower = (int) towerSettingsController.CurrentTowerType;
                    spatialMapItemData.EnabledUI = (int) towerSettingsController.CurrentUiType;
                }

                spatialMapData.mapItems.Add(spatialMapItemData);
            }

            m_MapData.Meta.Props = propInfos;
            if (isSaveToMetaManager)
            {
                MapMetaManager.Save(m_MapData.Meta);
            }

            ViewManager.Instance.SaveSpatialMap(spatialMapData, new IResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        ShowLoading(false);
                        ShowError(false);

                        gameObject.SetActive(false);
                        ViewManager.Instance.LoadMainView();
                    });
                },

                OnError = (errorMessage) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        ShowLoading(false);
                        ShowError(true);

                        //ShowError();
                    });
                }
            });

            //ViewManager.Instance.SaveSpatialMap(spatialMapData);
        }

        public void ShowError(bool isShow)
        {
            if (isShow)
            {
                ErrorPanel.SetActive(true);
            }
            else
            {
                ErrorPanel.SetActive(false);
            }
        }

        public void ShowLoading(bool isShow)
        {
            if (isShow)
            {
                ProgressPanel.SetActive(true);
            }
            else
            {
                ProgressPanel.SetActive(false);
            }
        }
    }
}