using System;
using System.Collections;
using System.Collections.Generic;
using SWS;
using UnityEngine;

namespace Weter
{
    public abstract class BaseFlow : ExtendedMonoBehaviour
    {
        public Action OnMovementEnd;
        public Action OnMovementStart;
        public Action OnMovementUpdate;

        public FlowGenerator FlowGenerator;

        [SerializeField] protected splineMove m_SplineMove;
        [SerializeField] protected GameObject m_TrailGameObject;
        [SerializeField] protected MeshRenderer m_FlowRenderer;
        [SerializeField] protected Animator m_FlowAnimator;
        [SerializeField] protected TrailRenderer m_TrailRenderer;
        
        //[SerializeField] protected float m_TrailWidth = 0.065f;

        protected float m_FactorSpeed = 1.0f;
        protected float m_FactorSize = 1.0f;

        protected float m_InitAlpha = 0.4f;
        protected float m_CurrentAlpha = 0.0f;
        protected float m_StepAlphaCoroutine = 0.012f;
        protected float m_StepAlpha = 0.004f;
        protected float m_DelayAlpha = 0.1f;

        protected float m_StepScale = 0.1f; //0.06f;

        protected bool m_IsFadeIn = false;
        protected bool m_IsFadeOut = false;

        protected override void InitComponents()
        {
            base.InitComponents();
            
            if (!m_SplineMove)
            {
                m_SplineMove = GetComponent<splineMove>();
            }

            if (!m_TrailGameObject)
            {
                m_TrailGameObject = transform.GetChild(0).gameObject;
            }

            if (!m_FlowRenderer)
            {
                m_FlowRenderer = m_TrailGameObject.GetComponent<MeshRenderer>();
            }

            if (!m_TrailRenderer)
            {
                m_TrailRenderer = m_TrailGameObject.GetComponent<TrailRenderer>();
            }

            if (!m_FlowAnimator)
            {
                m_FlowAnimator = m_TrailRenderer.GetComponent<Animator>();
            }

            if (!FlowGenerator)
            {
                FlowGenerator = GetComponentInParent<FlowGenerator>();
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            InitComponents();
        }

        protected virtual void Awake()
        {
            InitComponents();
        }

        protected virtual void Start()
        {
            //m_TrailRenderer.widthMultiplier = m_TrailWidth;
            //m_TrailRenderer.widthMultiplier = 0.05f;
        }

        protected virtual void Update()
        {
            if (m_IsFadeIn)
            {
                m_CurrentAlpha += m_StepAlpha;
                if (m_CurrentAlpha >= m_InitAlpha)
                {
                    m_CurrentAlpha = m_InitAlpha;
                }

                var color = m_TrailRenderer.material.GetColor("_TintColor");
                Color newColor = new Color(color.r, color.g, color.b, m_CurrentAlpha);
                m_TrailRenderer.material.SetColor("_TintColor", newColor);

                if (m_CurrentAlpha >= m_InitAlpha)
                {
                    m_CurrentAlpha = m_InitAlpha;
                    m_IsFadeIn = false;
                }
            }

            if (m_IsFadeOut)
            {
                m_CurrentAlpha -= m_StepAlpha;
                if (m_CurrentAlpha <= 0f)
                {
                    m_CurrentAlpha = 0f;
                }

                var color = m_TrailRenderer.material.GetColor("_TintColor");
                Color newColor = new Color(color.r, color.g, color.b, m_CurrentAlpha);
                m_TrailRenderer.material.SetColor("_TintColor", newColor);

                if (m_CurrentAlpha <= 0f)
                {
                    m_CurrentAlpha = 0f;
                    m_IsFadeOut = false;
                }

                //transform.Translate(transform.forward * m_SplineMove.speed * Time.deltaTime, Space.Self);
            }
        }

        protected void FadeIn()
        {
            m_IsFadeIn = true;
            //StartCoroutine(FadeInCoroutine());
        }

        private IEnumerator FadeInCoroutine()
        {
            yield return new WaitForSeconds(m_DelayAlpha);
            if (m_IsFadeIn)
            {
                m_CurrentAlpha += m_StepAlpha;
                if (m_CurrentAlpha >= m_InitAlpha)
                {
                    m_CurrentAlpha = m_InitAlpha;
                }

                Color color = new Color(1f, 1f, 1f, m_CurrentAlpha);
                m_TrailRenderer.material.SetColor("_TintColor", color);

                if (m_CurrentAlpha >= m_InitAlpha)
                {
                    m_CurrentAlpha = m_InitAlpha;
                    m_IsFadeIn = false;
                }
                else
                {
                    StartCoroutine(FadeInCoroutine());
                }
            }
        }

        protected void FadeOut()
        {
            m_IsFadeOut = true;
            //StartCoroutine(FadeOutCoroutine());
        }

        protected IEnumerator FadeOutCoroutine()
        {
            yield return new WaitForSeconds(m_DelayAlpha);

            if (m_IsFadeOut)
            {
                m_CurrentAlpha -= m_StepAlpha;
                if (m_CurrentAlpha <= 0f)
                {
                    m_CurrentAlpha = 0f;
                }

                Color color = new Color(1f, 1f, 1f, (float)m_CurrentAlpha);
                m_TrailRenderer.material.SetColor("_TintColor", color);

                if (m_CurrentAlpha <= 0f)
                {
                    m_CurrentAlpha = 0f;
                    m_IsFadeOut = false;
                }
                else
                {
                    StartCoroutine(FadeOutCoroutine());
                }
            }
        }

        public virtual void UpdateSpeed(float speed)
        {
            m_SplineMove.speed = speed;
            m_SplineMove.ChangeSpeed(speed);

            //m_TrailRenderer.time = -0.95f * speed + 5f;
            m_TrailRenderer.time = -2.95f * speed + 6.3f;
            m_StepAlpha = 0.06f * speed - 0.002f;
        }

        public virtual void UpdateScale(float scale)
        {
            m_TrailRenderer.widthMultiplier = scale * m_StepScale;
        }

        abstract public void StartMove();

        abstract public void EndMove();
    }
}