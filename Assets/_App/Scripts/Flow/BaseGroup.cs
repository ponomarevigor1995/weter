using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class BaseGroup<T> : ExtendedMonoBehaviour
    {
        [SerializeField]
        public List<T> Group = new List<T>();

        protected override void InitComponents()
        {
            base.InitComponents();
            if (Group == null || Group.Count == 0)
            {
                Group.AddRange(transform.GetComponentsInChildren<T>());
            }
        }

        private void Awake()
        {
            InitComponents();
        }
    }
}
