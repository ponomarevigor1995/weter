using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Weter
{
    public class FlowGenerator : ExtendedMonoBehaviour
    {
        public BaseFlow PrefabMultipleFlowController;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!PrefabMultipleFlowController)
            {
                PrefabMultipleFlowController = GetComponentInChildren<BaseFlow>();
            }
        }

        private void Awake()
        {
            InitComponents();
            PrefabMultipleFlowController.gameObject.SetActive(false);
        }

        private void Start()
        {
            Create();
        }

        private void Update()
        {
        }

        public void Create()
        {
            BaseFlow newMultipleFlowController = Instantiate(PrefabMultipleFlowController,
                                                                       PrefabMultipleFlowController.transform.position,
                                                                       PrefabMultipleFlowController.transform.rotation,
                                                                       transform);

            newMultipleFlowController.gameObject.SetActive(true);
            newMultipleFlowController.FlowGenerator = this;
            newMultipleFlowController.OnMovementEnd += () => { Create(); };
        }

        public void UpdateSpeed(float speed)
        {
            BaseFlow[] flowControllers = transform.GetComponentsInChildren<BaseFlow>(true);
            foreach (var baseFlowController in flowControllers)
            {
                baseFlowController.UpdateSpeed(speed);
            }
        }

        public void UpdateScale(float scale)
        {
            BaseFlow[] flowControllers = transform.GetComponentsInChildren<BaseFlow>(true);
            foreach (var baseFlowController in flowControllers)
            {
                baseFlowController.UpdateScale(scale);
            }
        }

        public void Clear()
        {
            BaseFlow[] flowControllers = transform.GetComponentsInChildren<BaseFlow>(false);
            foreach (var baseFlowController in flowControllers)
            {
                if (baseFlowController == PrefabMultipleFlowController)
                {
                    continue;
                }

                Destroy(baseFlowController.gameObject);
            }
        }

        public void Destroy(float delay)
        {
        }
    }
}