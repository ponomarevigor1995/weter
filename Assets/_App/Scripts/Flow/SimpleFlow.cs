using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SWS;
using Unity.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Weter
{
    public class SimpleFlow : BaseFlow
    {
        public float MaxDelay = 6f;
        public int ActivatePosition = 1;
        public bool IsMoveToPath = false;
        public PathManager PathManager;

        //private float m_Offset = 0.5f;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (PathManager)
            {
                Vector3 newPosition = PathManager.GetWaypoint(0).localPosition;
                //newPosition.x += m_Offset;
                //newPosition.z += m_Offset;
                transform.localPosition = newPosition;
            }
        }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();

            if (ActivatePosition > 0)
            {
                EnableTrail(false);
            }

            if (MaxDelay > 0)
            {
                m_SplineMove.onStart = false;
                m_SplineMove.Stop();
                StartCoroutine(StartMoveCoroutine());
            }
            else
            {
                UpdatePath();
            }

            m_SplineMove.movementStartEvent += () =>
            {
                var color = m_TrailRenderer.material.GetColor("_TintColor");
                m_TrailRenderer.material.SetColor("_TintColor", new Color(color.r, color.g, color.b, 0f));

                //StartMove();
            };

            m_SplineMove.movementChangeEvent += position =>
            {
                if (ActivatePosition > 0 && position == ActivatePosition)
                {
                    EnableTrail(true);
                    FadeIn();
                }

                if (position == (int)(PathManager.GetWaypointCount() / 2))
                {
                    OnMovementEnd?.Invoke();
                }

                if (position == PathManager.GetWaypointCount() - 1)
                {
                    FadeOut();
                }
            };

            m_SplineMove.movementEndEvent += () => { EndMove(); };
        }

        protected override void Update()
        {
            base.Update();
        }

        private IEnumerator StartMoveCoroutine()
        {
            float delay = Random.Range(0, MaxDelay);
            yield return new WaitForSeconds(delay);
            m_SplineMove.ResetToStart();
            UpdatePath();
        }

        private void UpdatePath()
        {
            m_SplineMove.moveToPath = IsMoveToPath;
            m_SplineMove.SetPath(PathManager);
        }

        private void EnableTrail(bool isEnabled)
        {
            if (isEnabled)
            {
                m_TrailGameObject.SetActive(true);
            }
            else
            {
                m_TrailGameObject.SetActive(false);
            }
        }

        public override void StartMove()
        {
            //m_TrailGameObject.SetActive(true);
            m_FlowRenderer.enabled = true;
            m_FlowAnimator.enabled = true;
            m_TrailRenderer.Clear();
        }

        public override void EndMove()
        {
            //m_TrailGameObject.SetActive(false);
            m_FlowRenderer.enabled = false;
            m_FlowAnimator.enabled = false;

            // На конечную точку не работает
            // m_SplineMove.Pause(m_TrailRenderer.time);
            m_SplineMove.moveToPath = false;

            Destroy(gameObject, m_TrailRenderer.time);
            //StartCoroutine(PauseCoroutine(m_TrailRenderer.time));
        }

        private IEnumerator PauseCoroutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            //m_SplineMove.GoToWaypoint(0);
            m_SplineMove.StartMove();
        }
    }
}