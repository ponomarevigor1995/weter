using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;

namespace Weter
{
    public class TowerFlow : WindObserver
    {
        public bool UseShields = true;
        [SerializeField] private FlowLineGroup m_FlowLineGroupWithShieldController;
        [SerializeField] private FlowLineGroup m_FlowLineGroupWithoutShieldController;

        protected override void Awake()
        {
            base.Awake();
            m_FlowLineGroupWithoutShieldController.gameObject.SetActive(false);
            m_FlowLineGroupWithShieldController.gameObject.SetActive(false);
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public void EnableShields(bool useShields)
        {
            UseShields = useShields;
            if (useShields)
            {
                m_FlowLineGroupWithoutShieldController.gameObject.SetActive(false);
                m_FlowLineGroupWithShieldController.gameObject.SetActive(true);
            }
            else
            {
                m_FlowLineGroupWithoutShieldController.gameObject.SetActive(true);
                m_FlowLineGroupWithShieldController.gameObject.SetActive(false);
            }
        }

        public override void UpdateSpeed(float speed)
        {
            speed /= 5f;

            base.UpdateSpeed(speed);

            m_FlowLineGroupWithoutShieldController.UpdateSpeed(m_Speed);
            m_FlowLineGroupWithShieldController.UpdateSpeed(m_Speed);
        }

        public void UpdateScale(float scale)
        {
            m_FlowLineGroupWithoutShieldController.UpdateScale(scale);
            m_FlowLineGroupWithShieldController.UpdateScale(scale);
        }
    }
}