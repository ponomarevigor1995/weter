using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class FlowLine : ExtendedMonoBehaviour
    {
        [SerializeField] private bool m_IsValidate = true;
        [SerializeField] public PathGroup PathGroup;
        [SerializeField] public FlowGroup FlowGroup;

        protected override void InitComponents()
        {
            base.InitComponents();
            
            if (m_IsValidate)
            {
                if (!FlowGroup)
                {
                    FlowGroup = GetComponentInChildren<FlowGroup>();
                }

                if (!PathGroup)
                {
                    PathGroup = GetComponentInChildren<PathGroup>();
                }

                for (var index = 0; index < FlowGroup.Group.Count; index++)
                {
                    var windFlowGeneratorController = FlowGroup.Group[index];
                    if (index < PathGroup.Group.Count)
                    {
                        var pathManager = PathGroup.Group[index];
                        var baseFlowController = windFlowGeneratorController.PrefabMultipleFlowController;
                        if (baseFlowController is SimpleFlow)
                        {
                            var simpleFlowController = baseFlowController as SimpleFlow;
                            simpleFlowController.PathManager = pathManager;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
}