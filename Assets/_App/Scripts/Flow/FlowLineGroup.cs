using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class FlowLineGroup : BaseGroup<FlowLine>
    {
        public void UpdateSpeed(float speed)
        {
            foreach (var flowLineController in Group)
            {
                FlowGroup flowGroupController = flowLineController.FlowGroup;
                foreach (var flowGeneratorController in flowGroupController.Group)
                {
                    flowGeneratorController.UpdateSpeed(speed);
                }
            }
        }

        public void UpdateScale(float scale)
        {
            foreach (var flowLineController in Group)
            {
                FlowGroup flowGroupController = flowLineController.FlowGroup;
                foreach (var flowGeneratorController in flowGroupController.Group)
                {
                    flowGeneratorController.Clear();

                    flowGeneratorController.PrefabMultipleFlowController.UpdateScale(scale);
                    flowGeneratorController.Create();

                    //FlowGenerator.UpdateScale(Scale);
                }
            }
        }
    }
}