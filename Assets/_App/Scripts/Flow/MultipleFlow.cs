using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SWS;
using Unity.Collections;
using UnityEngine;

namespace Weter
{
    public class MultipleFlow : BaseFlow
    {
        public enum PathType
        {
            IN,
            ROTOR,
            OUT
        }

        public bool IsEnterRotor = false;

        public PathManager InPathManager;
        public PathManager OutPathManager;
        public PathManager RotorPathManager;

        private PathManager m_ActivePathManager;
        private PathType m_CurrentPathType = PathType.IN;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();

            m_ActivePathManager = InPathManager;
            m_CurrentPathType = PathType.IN;

            UpdatePath();

            m_SplineMove.movementStartEvent += () =>
            {

                switch (m_CurrentPathType)
                {
                    case PathType.IN:
                        break;
                    case PathType.ROTOR:
                        //m_FlowAnimator.enabled = false;
                        break;
                    case PathType.OUT:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                //StartMove();
            };

            m_SplineMove.movementChangeEvent += position =>
            {
                switch (m_CurrentPathType)
                {
                    case PathType.IN:
                        break;
                    case PathType.ROTOR:
                        break;
                    case PathType.OUT:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            };

            m_SplineMove.movementEndEvent += () =>
            {
                switch (m_CurrentPathType)
                {
                    case PathType.IN:
                        m_FlowAnimator.enabled = false;

                        if (IsEnterRotor)
                        {
                            m_ActivePathManager = RotorPathManager;
                            m_CurrentPathType = PathType.ROTOR;
                            UpdatePath();
                        }
                        else
                        {
                            m_ActivePathManager = OutPathManager;
                            m_CurrentPathType = PathType.OUT;
                            UpdatePath();
                        }

                        OnMovementEnd?.Invoke();
                        break;
                    case PathType.ROTOR:
                        m_FlowAnimator.enabled = true;

                        m_ActivePathManager = OutPathManager;
                        m_CurrentPathType = PathType.OUT;
                        UpdatePath();

                        //OnMovementEnd?.Invoke();
                        break;
                    case PathType.OUT:
                        EndMove();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                //EndMove();
            };
        }

        protected override void Update()
        {
            base.Update();
        }

        private void UpdatePath()
        {
            m_SplineMove.SetPath(m_ActivePathManager);
        }

        public override void StartMove()
        {
            //m_TrailGameObject.SetActive(true);
            m_FlowRenderer.enabled = true;
            m_FlowAnimator.enabled = true;
            m_TrailRenderer.Clear();
        }

        public override void EndMove()
        {
            //m_TrailGameObject.SetActive(false);
            m_FlowRenderer.enabled = false;
            m_FlowAnimator.enabled = false;

            // На конечную точку не работает
            // m_SplineMove.Pause(m_TrailRenderer.time);
            m_SplineMove.moveToPath = false;

            Destroy(gameObject, m_TrailRenderer.time);
            //StartCoroutine(PauseCoroutine(m_TrailRenderer.time));
        }

        private IEnumerator PauseCoroutine(float delay)
        {
            yield return new WaitForSeconds(delay);
            //m_SplineMove.GoToWaypoint(0);
            m_SplineMove.StartMove();
        }
    }
}