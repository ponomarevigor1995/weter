using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class TowerTransparentController : MonoBehaviour
    {
        public GameObject Tower;
        public Material TransparentMaterial;
        public bool OnStart = true;
        
        private List<Material> m_InitMaterials = new List<Material>();
        private List<MeshRenderer> m_MeshRenderers = new List<MeshRenderer>();
        private bool m_IsTransparent = false;
        
        
        private void Awake()
        {
        }

        private void Start()
        {
            if (Tower != null)
            {
                m_MeshRenderers.AddRange(Tower.transform.GetComponentsInChildren<MeshRenderer>());
                foreach (var meshRenderer in m_MeshRenderers)
                {
                    m_InitMaterials.AddRange(meshRenderer.materials);
                }
            }

            if (OnStart)
            {
                SetTransparent(true);
            }
        }

        private void Update()
        {
        }


        public void SetTransparent(bool isTransparent)
        {
            m_IsTransparent = isTransparent;
            
            int materialPosition = 0;
            for (int i = 0; i < m_MeshRenderers.Count; i++)
            {
                var meshRenderer = m_MeshRenderers[i];
                var materials = new Material[meshRenderer.materials.Length];
                for (int j = 0; j < meshRenderer.materials.Length; j++)
                {
                    if (isTransparent)
                    {
                        materials[j] = TransparentMaterial;
                    }
                    else
                    {
                        materials[j] = m_InitMaterials[materialPosition];
                    }

                    materialPosition++;
                }

                meshRenderer.materials = materials;
            }
        }
    }
}