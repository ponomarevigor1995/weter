using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Weter;

public class TowerSettingsUiController : MonoBehaviour
{
    public ToggleGroup UiToggleGroup;
    public ToggleGroup HexToggleGroup;
    public ToggleGroup TowerToggleGroup;
    public ToggleGroup ShieldToggleGroup;
    public ToggleGroup FlowToggleGroup;


    private void Start()
    {
    }

    private void Update()
    {
    }

    public void SetUi(TowerSettings.UiType uiType)
    {
        int activePosition = 0;
        switch (uiType)
        {
            case TowerSettings.UiType.ENABLE:
                activePosition = 0;
                break;
            case TowerSettings.UiType.DISABLE:
                activePosition = 1;
                break;
        }

        UiToggleGroup.transform.GetChild(activePosition).GetComponent<Toggle>().isOn = true;
    }

    public void SetHex(TowerSettings.HexType hexType)
    {
        int activePosition = 0;
        switch (hexType)
        {
            case TowerSettings.HexType.ENABLE:
                activePosition = 0;
                break;
            case TowerSettings.HexType.DISABLE:
                activePosition = 1;
                break;
        }

        HexToggleGroup.transform.GetChild(activePosition).GetComponent<Toggle>().isOn = true;
    }

    public void SetShield(TowerSettings.ShieldType shieldType)
    {
        int activePosition = 0;
        switch (shieldType)
        {
            case TowerSettings.ShieldType.ENABLE:
                activePosition = 0;
                break;
            case TowerSettings.ShieldType.DISABLE:
                activePosition = 1;
                break;
        }

        ShieldToggleGroup.transform.GetChild(activePosition).GetComponent<Toggle>().isOn = true;
    }

    public void SetTower(TowerSettings.TowerType towerType)
    {
        int activePosition = 0;
        switch (towerType)
        {
            case TowerSettings.TowerType.ENABLE:
                activePosition = 0;
                break;
            case TowerSettings.TowerType.DISABLE:
                activePosition = 1;
                break;
            case TowerSettings.TowerType.TRANSPARENT:
                activePosition = 2;
                break;
        }

        TowerToggleGroup.transform.GetChild(activePosition).GetComponent<Toggle>().isOn = true;
    }

    public void SetFlow(TowerSettings.FlowType flowType)
    {
        int activePosition = 0;
        switch (flowType)
        {
            case TowerSettings.FlowType.ENABLE_SHIELD_ON:
                activePosition = 0;
                //activePosition = 0;
                break;
            case TowerSettings.FlowType.ENABLE_SHIELD_OFF:
                activePosition = 0;
                //activePosition = 1;
                break;
            case TowerSettings.FlowType.ENABLE:
                activePosition = 0;
                //activePosition = 2;
                break;
            case TowerSettings.FlowType.DISABLE:
                activePosition = 1;
                //activePosition = 2;
                break;
        }

        FlowToggleGroup.transform.GetChild(activePosition).GetComponent<Toggle>().isOn = true;
    }
}