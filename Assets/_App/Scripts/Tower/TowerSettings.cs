using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZBoom.Common.SpatialMap;

namespace Weter
{
    public class TowerSettings : MonoBehaviour
    {
        #region TowerGameObject Enum Settings

        public enum UiType
        {
            DISABLE = 0,
            ENABLE = 1
        }

        public enum TowerType
        {
            DISABLE = 0,
            ENABLE = 1,
            TRANSPARENT = 2
        }

        public enum HexType
        {
            DISABLE = 0,
            ENABLE = 1
        }

        public enum ShieldType
        {
            DISABLE = 0,
            ENABLE = 1
        }

        public enum FlowType
        {
            DISABLE = 0,
            ENABLE = 1,
            ENABLE_SHIELD_OFF = 2,
            ENABLE_SHIELD_ON = 3
        }

        #endregion

        #region TowerGameObject

        public GameObject TowerGameObject;
        public GameObject RotorGameObject;
        public Material TransparentMaterial;

        private List<Material> m_InitMaterials = new List<Material>();
        private List<MeshRenderer> m_MeshRenderers = new List<MeshRenderer>();
        private bool m_IsTransparent = false;
        [HideInInspector] public TowerType CurrentTowerType = TowerType.ENABLE;

        #endregion

        #region UiGameObject

        public GameObject UiGameObject;
        [HideInInspector] public UiType CurrentUiType = UiType.ENABLE;

        #endregion

        #region Hex

        public GameObject HexGameObject;
        [HideInInspector] public HexType CurrentHexType = HexType.ENABLE;

        #endregion

        #region Shields

        public GameObject ShieldGameObject;
        [HideInInspector] public ShieldType CurrentShieldType = ShieldType.ENABLE;

        #endregion

        #region Wind

        public TowerFlow FlowController;
        [HideInInspector] public FlowType CurrentFlowType = FlowType.DISABLE;

        #endregion

        public TowerSettingsUiController TowerSettingsUiController;
        public GameObject SettingsCanvas;

        public SpatialMapItemData TowerData = new SpatialMapItemData();
        private bool m_IsInit = false;

        private void Start()
        {
            //yield return new WaitForSeconds(5f);
            /*
            SetData(new SpatialMapItemData()
            {
                EnabledUI = 0,
                EnabledFlow = 0,
                EnabledShield = 0,
                EnabledHex = 0,
                EnabledTower = 0,
            });
            */
            //SetData(TowerData);

            //UnComment for old versions

            /*
            if (TowerData == null || TowerData.Id.Length == 0)
            {
                Debug.Log("TowerSettings: " + "TowerData == null, size = ");

                TowerData = new SpatialMapItemData()
                {
                    EnabledUI = (int)TowerSettings.UiType.ENABLE,
                    EnabledTower = (int)TowerSettings.TowerType.ENABLE,
                    EnabledHex = (int)TowerSettings.HexType.ENABLE,
                    EnabledShield = (int)TowerSettings.ShieldType.ENABLE,
                    EnabledFlow = (int)TowerSettings.FlowType.ENABLE,
                };
            }
            else
            {
                Debug.Log("TowerSettings: " + "TowerData != null, size = " 
                                                      + TowerData.Id.Length+"towerData="+ TowerData.EnabledFlow);
            }

            if (!m_IsInit)
            {
                SetData(TowerData);
            }
                        */

            SetData(TowerData);
        }

        private void OnEnable()
        {
            /*
            if (TowerData != null && !m_IsInit)
            {
                SetData(TowerData);
            }
            */
        }

        private void Init()
        {
            if (TowerGameObject != null)
            {
                m_MeshRenderers.AddRange(TowerGameObject.transform.GetComponentsInChildren<MeshRenderer>());
                foreach (var meshRenderer in m_MeshRenderers)
                {
                    m_InitMaterials.AddRange(meshRenderer.materials);
                }
            }
        }

        private void Update()
        {
        }

        public void ShowSettingsCanvas(bool isShow)
        {
            SettingsCanvas.SetActive(isShow);
        }


        public void SetTransparent(bool isTransparent)
        {
            m_IsTransparent = isTransparent;

            int materialPosition = 0;
            for (int i = 0; i < m_MeshRenderers.Count; i++)
            {
                var meshRenderer = m_MeshRenderers[i];
                var materials = new Material[meshRenderer.materials.Length];
                for (int j = 0; j < meshRenderer.materials.Length; j++)
                {
                    if (isTransparent)
                    {
                        materials[j] = TransparentMaterial;
                    }
                    else
                    {
                        materials[j] = m_InitMaterials[materialPosition];
                    }

                    materialPosition++;
                }

                meshRenderer.materials = materials;
            }

            if (isTransparent)
            {
                RotorGameObject.SetActive(false);
            }
            else
            {
                RotorGameObject.SetActive(true);
            }
        }

        public void SetData(SpatialMapItemData itemData)
        {
            Init();

            TowerData = itemData;

            SetTower(TowerData.EnabledTower);
            TowerSettingsUiController.SetTower(CurrentTowerType);

            SetUi(TowerData.EnabledUI);
            TowerSettingsUiController.SetUi(CurrentUiType);

            SetShield(TowerData.EnabledShield);
            TowerSettingsUiController.SetShield(CurrentShieldType);

            SetFlow(TowerData.EnabledFlow);
            TowerSettingsUiController.SetFlow(CurrentFlowType);

            SetHex(TowerData.EnabledHex);
            TowerSettingsUiController.SetHex(CurrentHexType);

            m_IsInit = true;
        }

        public void SwitchUi(int towerDataEnableUI)
        {
            if ((int)CurrentUiType == towerDataEnableUI)
            {
                return;
            }

            SetUi(towerDataEnableUI);
        }

        public void SetUi(int towerDataEnableUI)
        {
            switch (towerDataEnableUI)
            {
                case ((int)UiType.ENABLE):
                {
                    UiGameObject.SetActive(true);
                    CurrentUiType = UiType.ENABLE;
                    break;
                }
                case ((int)UiType.DISABLE):
                {
                    UiGameObject.SetActive(false);
                    CurrentUiType = UiType.DISABLE;
                    break;
                }
            }
        }

        public void SwitchFlow(int towerDataEnableFlow)
        {
            if ((int)CurrentFlowType == towerDataEnableFlow)
            {
                return;
            }

            SetFlow(towerDataEnableFlow);
        }

        public void SetFlow(int towerDataEnableFlow)
        {
            switch (towerDataEnableFlow)
            {
                case ((int)FlowType.DISABLE):
                {
                    FlowController.gameObject.SetActive(false);
                    CurrentFlowType = FlowType.DISABLE;
                    break;
                }
                case ((int)FlowType.ENABLE):
                {
                    FlowController.gameObject.SetActive(true);
                    FlowController.UpdateScale(transform.localScale.x);

                    CurrentFlowType = FlowType.ENABLE;

                    if (CurrentShieldType == ShieldType.ENABLE)
                    {
                        FlowController.gameObject.SetActive(true);
                        FlowController.EnableShields(true);
                        //CurrentFlowType = FlowType.ENABLE_SHIELD_ON;
                    }
                    else
                    {
                        if (CurrentShieldType == ShieldType.DISABLE)
                        {
                            FlowController.gameObject.SetActive(true);
                            FlowController.EnableShields(false);
                            //CurrentFlowType = FlowType.ENABLE_SHIELD_OFF;
                        }
                    }

                    break;
                }
                /*
                case ((int) FlowType.ENABLE_SHIELD_ON):
                {
                    FlowController.gameObject.SetActive(true);
                    FlowController.EnableShields(true);
                    CurrentFlowType = FlowType.ENABLE_SHIELD_ON;
                    break;
                }
                case ((int) FlowType.ENABLE_SHIELD_OFF):
                {
                    FlowController.gameObject.SetActive(true);
                    FlowController.EnableShields(false);
                    CurrentFlowType = FlowType.ENABLE_SHIELD_OFF;
                    break;
                }
                */
            }
        }

        public void SwitchShield(int towerDataEnableShield)
        {
            if ((int)CurrentShieldType == towerDataEnableShield)
            {
                return;
            }

            SetShield(towerDataEnableShield);
        }

        public void SetShield(int towerDataEnableShield)
        {
            switch (towerDataEnableShield)
            {
                case ((int)ShieldType.ENABLE):
                {
                    ShieldGameObject.SetActive(true);
                    CurrentShieldType = ShieldType.ENABLE;

                    SetFlow((int)CurrentFlowType);
                    break;
                }
                case ((int)ShieldType.DISABLE):
                {
                    ShieldGameObject.SetActive(false);
                    CurrentShieldType = ShieldType.DISABLE;

                    SetFlow((int)CurrentFlowType);
                    break;
                }
            }
        }

        public void SwitchHex(int towerDataEnableHex)
        {
            if ((int)CurrentHexType == towerDataEnableHex)
            {
                return;
            }

            SetHex(towerDataEnableHex);
        }

        public void SetHex(int towerDataEnableHex)
        {
            switch (towerDataEnableHex)
            {
                case ((int)HexType.ENABLE):
                {
                    HexGameObject.SetActive(true);
                    CurrentHexType = HexType.ENABLE;
                    break;
                }
                case ((int)HexType.DISABLE):
                {
                    HexGameObject.SetActive(false);
                    CurrentHexType = HexType.DISABLE;
                    break;
                }
            }
        }

        public void SwitchTower(int towerDataEnableTower)
        {
            if ((int)CurrentTowerType == towerDataEnableTower)
            {
                return;
            }

            SetTower(towerDataEnableTower);
        }

        public void SetTower(int towerDataEnableTower)
        {
            switch (towerDataEnableTower)
            {
                case ((int)TowerType.DISABLE):
                {
                    TowerGameObject.SetActive(false);
                    CurrentTowerType = TowerType.DISABLE;
                    break;
                }
                case ((int)TowerType.ENABLE):
                {
                    TowerGameObject.SetActive(true);
                    SetTransparent(false);
                    CurrentTowerType = TowerType.ENABLE;
                    break;
                }
                case ((int)TowerType.TRANSPARENT):
                {
                    TowerGameObject.SetActive(true);
                    SetTransparent(true);
                    CurrentTowerType = TowerType.TRANSPARENT;
                    break;
                }
            }
        }
    }
}