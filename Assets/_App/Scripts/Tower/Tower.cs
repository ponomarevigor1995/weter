using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class Tower : WindObserver
    {
        [SerializeField] public bool UseDefence = false;

        [SerializeField] private GameObject m_Tower;
        [SerializeField] private GameObject m_Rotor;
        [SerializeField] private GameObject m_Defence;
        [SerializeField] private GameObject m_Hex;

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
            if (UseDefence)
            {
                m_Rotor.transform.RotateAround(m_Rotor.transform.position, Vector3.up, -m_Speed * Time.deltaTime);
            }
            else
            {
                m_Rotor.transform.RotateAround(m_Rotor.transform.position, Vector3.up, -m_DefenceSpeed * Time.deltaTime);
            }
        }

        public override void UpdateSpeed(float speed)
        {
            speed *= 5f;
            base.UpdateSpeed(speed);
        }
        
        public void EnableShields(bool useShields)
        {
            UseDefence = useShields;
            if (useShields)
            {
                m_Defence.SetActive(true);
            }
            else
            {
                m_Defence.SetActive(false);
            }
        }

        public void ShowHex(bool isShow)
        {
            if (m_Hex != null)
            {
                m_Hex.SetActive(isShow);
            }
        }
    }
}