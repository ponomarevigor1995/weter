using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerElementController : MonoBehaviour
{
    private MeshRenderer m_MeshRenderer;
    [SerializeField] private Material m_DefaultElementMaterial;
    [SerializeField] private Material m_TransparentElementMaterial;

    private int m_MaterialCount = 1; 
    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
        m_MaterialCount = m_MeshRenderer.materials.Length;
    }

    public void UpdateMaterial(bool isShow)
    {
        Material[] materials = new Material[m_MaterialCount];
        Material newMaterial;
        if (!isShow)
        {
            newMaterial = m_TransparentElementMaterial;
        }
        else
        {
            newMaterial = m_DefaultElementMaterial;
        }
        
        for (var i = 0; i < materials.Length; i++)
        {
            materials[i] = newMaterial;
        }

        m_MeshRenderer.material = newMaterial;
        m_MeshRenderer.materials = materials;
    }
    
    public void UpdateMainMaterial(bool isShow)
    {
        if (!isShow)
        {
            m_MeshRenderer.material = m_TransparentElementMaterial;
        }
        else
        {
            m_MeshRenderer.material = m_DefaultElementMaterial;
        }
    }
}