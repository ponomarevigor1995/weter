﻿using System;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;

namespace Weter
{
    public abstract class BaseGameSession : ExtendedMonoBehaviour, IWindObservable, ILightObservable
    {
        [Header("Wind")]
        [SerializeField] protected float m_InitWindValue = 1f;
        [SerializeField] protected bool m_UseShields = true;
        
        protected List<IWindObserver> m_WindObservers = new List<IWindObserver>();
        protected List<ILightObserver> m_LightObservers = new List<ILightObserver>();

        protected Camera m_CameraTarget;

        protected override void InitComponents()
        {
            base.InitComponents();
        }

        protected virtual void Awake()
        {
            InitComponents();
            m_CameraTarget = Camera.main;
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }

        public virtual void SwitchShields()
        {
            m_UseShields = !m_UseShields;
            EnableShields(m_UseShields);
        }

        public virtual void EnableShields(bool useShields)
        {
            m_UseShields = useShields;
        }

        #region Wind Observer

        public void UpdateWindSpeed(float speed)
        {
            UpdateWindObservers(speed);
        }

        public void AddWindObserver(IWindObserver observer)
        {
            m_WindObservers.Add(observer);
        }

        public void RemoveWindObserver(IWindObserver observer)
        {
            m_WindObservers.Remove(observer);
        }

        public void UpdateWindObservers(float speed)
        {
            foreach (var windObserver in m_WindObservers)
            {
                windObserver.UpdateSpeed(speed);
            }
        }

        #endregion

        #region Light Observer

        public void UpdateLight(float light)
        {
            UpdateLightObservers(light);
        }

        public void AddLightObserver(ILightObserver observer)
        {
            m_LightObservers.Add(observer);
        }

        public void RemoveLightObserver(ILightObserver observer)
        {
            m_LightObservers.Remove(observer);
        }

        public void UpdateLightObservers(float light)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLight(light);
            }
        }

        public void UpdateScaleLightObservers(float scale)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLightScale(scale);
            }
        }

        #endregion
    }
}