namespace Weter
{
    public interface IWindObserver
    {
        public void UpdateSpeed(float speed);
    }
}