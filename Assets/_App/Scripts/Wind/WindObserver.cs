using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;
using Weter.Slam;

namespace Weter
{
    public class WindObserver : ExtendedMonoBehaviour, IWindObserver
    {
        [SerializeField] private GameObject m_WindGameObject;

        [SerializeField] protected float m_Speed = 10f;
        [SerializeField] protected float m_DefenceSpeed = 10f;
        [SerializeField] protected float m_MinSpeed = 1f;
        [SerializeField] protected float m_MaxSpeed = 200f;

        protected IWindObservable m_WindObservable;
        
        protected override void InitComponents()
        {
            base.InitComponents();
            
            if (!m_WindGameObject)
            {
                //m_LightObserver = m_LightGameObject.GetComponent<ILightObservable>();
                m_WindGameObject = GameObject.Find("WorldController");
                if (!m_WindGameObject)
                {
                    m_WindGameObject = GameObject.Find("GameSession");
                    //m_WindGameObject = FindObjectOfType<GameSession>().gameObject;
                }
            }
        }
        
        protected override void OnValidate()
        {
            base.OnValidate();
            ValidateHelper.Validate<IWindObservable>(m_WindGameObject);
        }
        
        protected virtual void Awake()
        {
            InitComponents();
            
            if (m_WindGameObject != null)
            {
                m_WindObservable = m_WindGameObject.GetComponent<IWindObservable>();
                m_WindObservable.AddWindObserver(this);
            }
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }

        public virtual void UpdateSpeed(float speed)
        {
            m_Speed = speed;
            if (m_Speed > m_MaxSpeed)
            {
                m_Speed = m_MaxSpeed;
            }

            if (m_Speed < m_MinSpeed)
            {
                m_Speed = m_MinSpeed;
            }
            if (!enabled)
            {
                return;
            }
        }
    }
}