﻿namespace Weter
{
    public interface IWindObservable
    {
        public void AddWindObserver(IWindObserver observer);
        public void RemoveWindObserver(IWindObserver observer);
        public void UpdateWindObservers(float speed);
    }
}