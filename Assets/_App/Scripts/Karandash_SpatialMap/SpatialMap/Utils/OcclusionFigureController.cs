using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZBoom.Common.SpatialMap;

namespace Weter
{
    public class OcclusionFigureController : MonoBehaviour
    {
        public Material EditMaterial;
        public Material PreviewMaterial;

        private ObjectAR m_ObjectAR;
        private MeshRenderer m_MeshRenderer;

        private void Awake()
        {
            m_ObjectAR = GetComponent<ObjectAR>();
            m_MeshRenderer = GetComponent<MeshRenderer>();

            if (m_ObjectAR != null)
            {
                m_ObjectAR.OnIdle += () => { };
                m_ObjectAR.OnEdit += () => { m_MeshRenderer.material = EditMaterial; };
                m_ObjectAR.OnPreview += () => { m_MeshRenderer.material = PreviewMaterial; };
            }
        }

        private void Update()
        {
        }
    }
}