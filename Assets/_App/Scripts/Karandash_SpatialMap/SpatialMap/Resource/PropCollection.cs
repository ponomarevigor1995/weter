﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ZBoom.Common.SpatialMap
{
    public class PropCollection : MonoBehaviour
    {
        public static PropCollection Instance;
        public List<TemplateGroup> TemplateGroups = new List<TemplateGroup>();
        [HideInInspector] public List<Template> Templates = new List<Template>();

        private void Awake()
        {
            Instance = this;
            Templates.Clear();
            foreach (var group in PropCollection.Instance.TemplateGroups)
            {
                foreach (var template in group.Templates)
                {
                    Templates.Add(template);
                }
            }
        }

        [Serializable]
        public class Template
        {
            public GameObject Object;
            public Sprite Icon;
            public string Id;
            public string Name;
        }

        [Serializable]
        public class TemplateGroup {
            public string Name;
            public List<Template> Templates = new List<Template>();
        }
    }
}
