using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;
using Firebase.Extensions;
using Newtonsoft.Json;

namespace ZBoom.Common.SpatialMap
{
    public class SpatialMapFirebaseRepository : MonoBehaviour
    {
        private static string MESSAGE_ERROR = "ERROR";
        private static string MESSAGE_SUCCESS = "SUCCESS";

        [SerializeField] private string m_SpatialMapsReferenceName = "spatial_maps";
        private DatabaseReference m_DatabaseReference;

        private void Awake()
        {
            m_DatabaseReference = FirebaseDatabase.DefaultInstance.GetReference(m_SpatialMapsReferenceName);
        }

        public void GetSpatialMap(ResultListener<List<SpatialMapData>> resultListener)
        {
            m_DatabaseReference
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        resultListener.OnError(MESSAGE_ERROR);
                    }
                    else
                    {
                        if (task.IsCompleted)
                        {
                            List<SpatialMapData> maps = new List<SpatialMapData>();
                            DataSnapshot snapshot = task.Result;
                            if (snapshot.ChildrenCount > 0)
                            {
                                IEnumerator<DataSnapshot> children = snapshot.Children.GetEnumerator();
                                while (children.MoveNext())
                                {
                                    string json = children.Current.GetRawJsonValue();
                                    //SpatialMapData spatialMapData = JsonUtility.FromJson<SpatialMapData>(json);
                                    SpatialMapData spatialMapData = JsonConvert.DeserializeObject<SpatialMapData>(json);

                                    maps.Add(spatialMapData);
                                }

                                resultListener.OnSuccess(maps, MESSAGE_SUCCESS);
                            }
                            else
                            {
                                resultListener.OnSuccess(maps, MESSAGE_ERROR);
                            }
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                });
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData,
                                   ResultListener<SpatialMapData> resultListener)
        {
            //string json = JsonUtility.ToJson(spatialMapData);
            string json = JsonConvert.SerializeObject(spatialMapData);

            m_DatabaseReference
                .Child(spatialMapData.Id)
                .SetRawJsonValueAsync(json)
                .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }

                        if (task.IsCompleted)
                        {
                            resultListener.OnSuccess(spatialMapData, MESSAGE_SUCCESS);
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                );
        }

        public void RemoveSpatialMap(string idSpatialMap,
                                     ResultListener<SpatialMapData> resultListener)
        {
            m_DatabaseReference
                .Child(idSpatialMap)
                .RemoveValueAsync()
                .ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }

                        if (task.IsCompleted)
                        {
                            resultListener.OnSuccess(null, MESSAGE_SUCCESS);
                        }
                        else
                        {
                            resultListener.OnError(MESSAGE_ERROR);
                        }
                    }
                );
        }
    }
}