﻿using System;
using Newtonsoft.Json;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    [Serializable]
    public class SpatialMapItemData
    {
        [JsonProperty("id")]
        public string Id = "";
        
        [JsonProperty("name")]
        public string Name = "";

        [JsonProperty("position")]
        public float[] Position = new float[3];

        [JsonProperty("rotation")]
        public float[] Rotation = new float[4];

        [JsonProperty("euler_rotation")]
        public float[] EulerRotation = new float[3];

        [JsonProperty("scale")]
        public float[] Scale = new float[3];

        #region Tower Settings

        [JsonProperty("enabled_ui")]
        public int EnabledUI = (int)TowerSettings.UiType.ENABLE;
        [JsonProperty("enabled_tower")]
        public int EnabledTower = (int)TowerSettings.TowerType.ENABLE;
        [JsonProperty("enabled_hex")]
        public int EnabledHex = (int)TowerSettings.HexType.ENABLE;
        [JsonProperty("enabled_shield")]
        public int EnabledShield = (int)TowerSettings.ShieldType.ENABLE;
        [JsonProperty("enabled_flow")]
        public int EnabledFlow = (int)TowerSettings.FlowType.ENABLE;

        #endregion

        public SpatialMapItemData()
        {
        }
    }
}