﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ZBoom.Common.SpatialMap
{
    [Serializable]
    public class SpatialMapData
    {
        [JsonProperty("id")]
        public string Id = "";
        [JsonProperty("name")]
        public string Name = "";
        [JsonProperty("create_at")]
        public string CreateAt = "";
        [JsonProperty("update_at")]
        public string UpdateAt = "";
        [JsonProperty("items")]
        public List<SpatialMapItemData> MapItems = new List<SpatialMapItemData>();

        public SpatialMapData()
        {
        }
    }
}