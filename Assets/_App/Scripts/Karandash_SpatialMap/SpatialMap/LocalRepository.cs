﻿using UnityEngine;

namespace _App.Scripts.Karandash_SpatialMap.SpatialMap
{
    public class LocalRepository : MonoBehaviour
    {
        public static string KEY_SPATIAL_MAP_MODE = "KEY_SPATIAL_MAP_MODE";

        public void SetAdminMode()
        {
            PlayerPrefs.SetInt(KEY_SPATIAL_MAP_MODE, 1);    
            PlayerPrefs.Save();
        }
        
        public void SetClientMode()
        {
            PlayerPrefs.SetInt(KEY_SPATIAL_MAP_MODE, 0);    
            PlayerPrefs.Save();
        }

        public bool IsAdminMode()
        {
            int mode = PlayerPrefs.GetInt(KEY_SPATIAL_MAP_MODE, 0);
            return mode == 1;
        }
    }
}