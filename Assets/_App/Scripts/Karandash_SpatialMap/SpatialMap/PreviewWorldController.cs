﻿//================================================================================================================================
//
//  Copyright (c) 2015-2021 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
//  EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
//  and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

using easyar;
using System;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;
using Weter;
using Weter.Slam;
using ZBoom.Common.SpatialMap;

namespace Weter
{
    public class PreviewWorldController : MonoBehaviour, IWindObservable, ILightObservable
    {
        [SerializeField] public PreviewPlayUI UiController;

        [SerializeField] public SliderManager WindSliderManager;
        [SerializeField] public bool UseShields = true;

        public GameObject EasyARSession;
        public SpatialMapFirebaseController FirebaseController;
        public SparseSpatialMapController MapControllerPrefab;

        public GameObject ProgressPanel;
        public GameObject ErrorPanel;

        private GameObject m_EasyarObject;
        private ARSession m_Session;
        private VIOCameraDeviceUnion m_VioCamera;
        private SparseSpatialMapWorkerFrameFilter m_MapFrameFilter;
        private List<MapMeta> m_SelectedMaps = new List<MapMeta>();
        private MapSession m_MapSession;

        private List<IWindObserver> m_WindObservers = new List<IWindObserver>();
        private List<ILightObserver> m_LightObservers = new List<ILightObserver>();
        private List<TowerFlow> m_FlowControllers = new List<TowerFlow>();
        private List<Tower> m_TowerControllers = new List<Tower>();
        private List<TowerSettings> m_TowerSettingsControllers = new List<TowerSettings>();


#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
        static void ImportSampleStreamingAssets()
        {
            FileUtil.ImportSampleStreamingAssets();
        }
#endif

        private void Awake()
        {
        }

        private void Start()
        {
            GetSpatialMaps();
        }

        private void Update()
        {
        }

        private void OnDestroy()
        {
            DestroySession();
        }

        private void CreateSession()
        {
            m_EasyarObject = Instantiate(EasyARSession);
            m_EasyarObject.SetActive(true);
            m_Session = m_EasyarObject.GetComponent<ARSession>();
            m_VioCamera = m_EasyarObject.GetComponentInChildren<VIOCameraDeviceUnion>();
            m_MapFrameFilter = m_EasyarObject.GetComponentInChildren<SparseSpatialMapWorkerFrameFilter>();

            m_MapSession = new MapSession(m_Session, m_MapFrameFilter, m_SelectedMaps);
        }

        private void DestroySession()
        {
            if (m_MapSession != null)
            {
                m_MapSession.Dispose();
                m_MapSession = null;
            }

            if (m_EasyarObject)
            {
                Destroy(m_EasyarObject);
            }
        }

        public void SetActive()
        {
            //m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);

            UpdateWindSpeed(WindSliderManager.mainSlider.value); //

            m_FlowControllers.AddRange(FindObjectsOfType<TowerFlow>());
            m_TowerControllers.AddRange(FindObjectsOfType<Tower>());

            /*
            m_FlowControllers.AddRange(FindObjectsOfType<m_TowerFlow>());
            m_TowerControllers.AddRange(FindObjectsOfType<m_Tower>());

            for (int i = 0; i < m_TowerControllers.Count; i++)
            {
                m_FlowControllers[i].UpdateScale(m_TowerControllers[i].transform.localScale.x);
            }

            for (int i = 0; i < m_FlowControllers.Count; i++)
            {
                m_FlowControllers[i].UpdateScale(m_TowerControllers[i].transform.localScale.x);
            }
            */

            m_TowerSettingsControllers.AddRange(FindObjectsOfType<TowerSettings>(true));

            for (int i = 0; i < m_TowerSettingsControllers.Count; i++)
            {
                m_TowerSettingsControllers[i].ShowSettingsCanvas(false);
            }
        }


        public void GetSpatialMaps()
        {
            ProgressPanel.SetActive(true);
            ErrorPanel.SetActive(false);

            FirebaseController.GetSpatialMap(new IResultListener<List<SpatialMapData>>()
            {
                OnError = errorMessage =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        ProgressPanel.SetActive(false);
                        ErrorPanel.SetActive(true);
                    });
                },
                OnSuccess = (spatialMaps, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        foreach (var spatialMapData in spatialMaps)
                        {
                            SparseSpatialMapController.SparseSpatialMapInfo spatialMapInfo =
                                new SparseSpatialMapController.SparseSpatialMapInfo();
                            spatialMapInfo.Name = spatialMapData.name;
                            spatialMapInfo.ID = spatialMapData.id;

                            List<MapMeta.PropInfo> props = new List<MapMeta.PropInfo>();

                            foreach (var spatialMapItemData in spatialMapData.mapItems)
                            {
                                MapMeta.PropInfo propInfo = new MapMeta.PropInfo();
                                propInfo.Name = spatialMapItemData.Id;

                                propInfo.Position = spatialMapItemData.Position;
                                propInfo.Rotation = spatialMapItemData.Rotation;
                                propInfo.Scale = spatialMapItemData.Scale;

                                propInfo.EnableFlow = spatialMapItemData.EnabledFlow;
                                propInfo.EnableHex = spatialMapItemData.EnabledHex;
                                propInfo.EnableShield = spatialMapItemData.EnabledShield;
                                propInfo.EnableTower = spatialMapItemData.EnabledTower;
                                propInfo.EnableUI = spatialMapItemData.EnabledUI;

                                props.Add(propInfo);
                            }

                            MapMeta mapMeta = new MapMeta(spatialMapInfo, props);
                            m_SelectedMaps.Add(mapMeta);
                        }

                        ProgressPanel.SetActive(false);
                        ErrorPanel.SetActive(false);


                        Clear();
                        CreateSession();
                        //m_MapSession.LoadMapMeta(SparseSpatialMapPrefab, false);

                        m_MapSession.LoadMapMeta(
                            MapControllerPrefab,
                            false,
                            objectCreatedEvent: ModifyObject,
                            //mapLoadEvent:PreviewMapLoad,
                            mapLocalized: () => SetActive()
                        );
                    });
                }
            });
        }

        private void ModifyObject(GameObject prop, MapMeta.PropInfo propInfo)
        {
            TowerSettings towerSettingsController = prop.GetComponent<TowerSettings>();
            if (towerSettingsController != null)
            {
                SpatialMapItemData spatialMapItemData = new SpatialMapItemData()
                {
                    Id = propInfo.Name,
                    Name = propInfo.Name,
                    Position = new float[3]
                    {
                        prop.transform.localPosition.x, prop.transform.localPosition.y,
                        prop.transform.localPosition.z
                    },
                    Rotation = new float[4]
                    {
                        prop.transform.localRotation.x, prop.transform.localRotation.y,
                        prop.transform.localRotation.z, prop.transform.localRotation.w
                    },
                    EulerRotation = new float[3]
                    {
                        prop.transform.localRotation.eulerAngles.x, prop.transform.localRotation.eulerAngles.y,
                        prop.transform.localRotation.eulerAngles.z
                    },
                    Scale = new float[3]
                        {prop.transform.localScale.x, prop.transform.localScale.y, prop.transform.localScale.z},

                    EnabledFlow = propInfo.EnableFlow,
                    EnabledHex = propInfo.EnableHex,
                    EnabledShield = propInfo.EnableShield,
                    EnabledTower = propInfo.EnableTower,
                    EnabledUI = propInfo.EnableUI
                };


                towerSettingsController.TowerData = spatialMapItemData;
                //towerSettingsController.SetData(spatialMapItemData);
            }
        }


        public void Clear()
        {
            m_LightObservers.Clear();
            m_WindObservers.Clear();

            m_TowerControllers.Clear();
            m_FlowControllers.Clear();

            m_TowerSettingsControllers.Clear();

            UiController.Clear();
        }

        public void UpdateWindSpeed(float speed)
        {
            UpdateWindObservers(speed);
        }

        public void EnableShields(bool useShields)
        {
            UseShields = useShields;

            foreach (var flowController in m_FlowControllers)
            {
                flowController.EnableShields(UseShields);
                //TODO
                //m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);
            }

            foreach (var towerController in m_TowerControllers)
            {
                towerController.EnableShields(UseShields);
            }
        }

        public void UpdateLight(float light)
        {
            UpdateLightObservers(light);
        }

        #region Wind Observer

        public void AddWindObserver(IWindObserver observer)
        {
            m_WindObservers.Add(observer);

           // UiController.EnableShields(true);
           // UiController.EnableWindSlider(true);
        }

        public void RemoveWindObserver(IWindObserver observer)
        {
            m_WindObservers.Remove(observer);
        }

        public void UpdateWindObservers(float speed)
        {
            foreach (var windObserver in m_WindObservers)
            {
                windObserver.UpdateSpeed(speed);
            }
        }

        #endregion

        #region Light Observer

        public void AddLightObserver(ILightObserver observer)
        {
            m_LightObservers.Add(observer);
        }

        public void RemoveLightObserver(ILightObserver observer)
        {
            m_LightObservers.Remove(observer);
        }

        public void UpdateLightObservers(float light)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLight(light);
            }
        }
        
        public void UpdateScaleLightObservers(float scale)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLightScale(scale);
            }
        }

        #endregion
    }
}