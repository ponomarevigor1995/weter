﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.UI;

namespace ZBoom.Common.SpatialMap
{
    public class SpatialMapGrid : MonoBehaviour
    {
        public SpatialMapItem SpatialMapItemPrefab;

        private RectTransform m_RectTransform;
        private List<SpatialMapItem> m_MapItems = new List<SpatialMapItem>();

        public int MapCellCount => m_MapItems.Count;

        private void OnEnable()
        {
            /*
            SetMapMeta(MapMetaManager.LoadAll());
            */
        }

        private void Start()
        {
            /*
            m_RectTransform = GetComponent<RectTransform>();
            var cellWidth = m_RectTransform.rect.width * 0.9f;
            var padding = (int) (cellWidth * 0.1f);
            */
        }

        private void Update()
        {
        }

        private void OnDisable()
        {
            /*
            foreach (var mapCellController in m_MapItems)
            {
                if (mapCellController)
                {
                    Destroy(mapCellController.gameObject);
                }
            }

            m_MapItems.Clear();
            */

            foreach (var mapCellController in m_MapItems)
            {
                mapCellController.Cancel();
            }
        }

        public void ClearAll()
        {
            // Notice:
            //   a) When clear both map cache and map list,
            //      load map will not trigger a download (cache is build when upload),
            //      and statistical request count will not be increased in a subsequent load (when edit or preview).
            //   b) When clear map cache only,
            //      load map after clear (only the first time each map) will trigger a download,
            //      and statistical request count will be increased in a subsequent load (when edit or preview).
            //      Map cache is used after a successful download and will be cleared if SparseSpatialMapManager.clear is called or app uninstalled.
            //
            // More about the statistical request count and limitations for different subscription mode can be found at EasyAR website.

            /*if (!ViewManager.Instance.MainViewRecycleBinClearMapCacheOnly)
            {
                foreach (var mapCellController in m_MapItems)
                {
                    if (mapCellController)
                    {
                        MapMetaManager.Delete(mapCellController.MapMeta);
                        Destroy(mapCellController.gameObject);
                    }
                }

                m_MapItems.Clear();
            }*/

            MapSession.ClearCache();
            OnMapCellChange();

            /*if (!SpatialMap_SparseSpatialMap.ViewManager.Instance.MainViewRecycleBinClearMapCacheOnly)
            {
                easyar.GUIPopup.EnqueueMessage(
                    "DELETED: {(Sample) Meta Data, Map Cache}" + Environment.NewLine +
                    "NOT DELETED: {Map on Server}" + Environment.NewLine +
                    "Use web develop center to manage maps on server", 5);
            }
            else
            {
                easyar.GUIPopup.EnqueueMessage(
                    "DELETED: {Map Cache}" + Environment.NewLine +
                    "NOT DELETED: {Map on Server, (Sample) Meta Data}" + Environment.NewLine +
                    "Use web develop center to manage maps on server", 5);
            }*/
        }

        private void OnMapCellChange()
        {
            ViewManager.Instance.SelectMaps(m_MapItems
                                            .Where(mapCellController =>
                                                       mapCellController && mapCellController.IsSelected)
                                            .Select(mapCellController => mapCellController.MapMeta)
                                            .ToList());
        }

        private void OnDelete(SpatialMapItem mapCellController)
        {
            ViewManager.Instance.MapMenuView.ShowLoading(true);

            MapMetaManager.Delete(mapCellController.MapMeta);
            //OnMapCellChange();

            //ViewManager.Instance.RemoveSpatialMap(meta.Map.ID, new ResultListener<SpatialMapData>()
            ViewManager.Instance.RemoveSpatialMap(mapCellController.SpatialMapData.Id,
                                                  new ResultListener<SpatialMapData>()
                                                  {
                                                      OnSuccess = (mapCellController, message) =>
                                                      {
                                                          UnityMainThreadDispatcher.Instance().Enqueue(() =>
                                                          {
                                                              ViewManager.Instance.MapMenuView.ShowLoading(false);
                                                          });
                                                      },
                                                      OnError = (message) =>
                                                      {
                                                          UnityMainThreadDispatcher.Instance().Enqueue(() =>
                                                          {
                                                              ViewManager.Instance.MapMenuView.ShowLoading(false);
                                                              ViewManager.Instance.MapMenuView.ShowError(true);
                                                          });
                                                      }
                                                  });

            m_MapItems.Remove(mapCellController);
            Destroy(mapCellController.gameObject);
        }

        public void SetData(List<MapMeta> mapMetas, List<SpatialMapData> spatialMaps)
        {
            foreach (SpatialMapItem mapItem in m_MapItems)
            {
                Destroy(mapItem.gameObject);
            }

            m_MapItems.Clear();

            for (var i = 0; i < mapMetas.Count; i++)
            {
                MapMeta meta = mapMetas[i];
                SpatialMapData spatialMap = spatialMaps[i];
                SpatialMapItem mapItem = Instantiate(SpatialMapItemPrefab, transform);
                mapItem.SetMapMeta(meta);
                mapItem.SetSpatialMapData(spatialMap);
                mapItem.PointerDownEvent += OnMapCellChange;
                mapItem.DeleteEvent += () => { OnDelete(mapItem); };
                /*mapCellController.DeleteEvent += () =>
                {
                    if (m_MapItems.Remove(mapCellController))
                    {
                        MapMetaManager.Delete(mapCellController.MapMeta);
                        Destroy(mapCellController.gameObject);
                        OnMapCellChange();

                        /*
                        easyar.GUIPopup.EnqueueMessage(
                            "DELETED: {(Sample) Meta Data}" + Environment.NewLine +
                            "NOT DELETED: {Map Cache, Map on Server}" + Environment.NewLine +
                            "Use recycle bin button to delete map cache" + Environment.NewLine +
                            "Use web develop center to manage maps on server", 5);
                        #1#

                        ViewManager.Instance.RemoveSpatialMap(meta.Map.ID, new ResultListener<SpatialMapData>()
                        {
                        });
                        
                        ViewManager.Instance.RemoveMesh(spatialMap, new ResultListener<string>()
                        {
                        });
                    }
                };*/
                m_MapItems.Add(mapItem);
            }
        }

        public void Delete()
        {
            foreach (SpatialMapItem mapItem in m_MapItems.ToList())
            {
                if (mapItem.IsSelected)
                {
                    OnDelete(mapItem);
                }
            }
        }
    }
}