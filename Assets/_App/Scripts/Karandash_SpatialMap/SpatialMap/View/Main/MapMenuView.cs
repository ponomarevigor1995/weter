﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class MapMenuView : BaseView
    {
        public Button EditButton;
        public Button PreviewButton;
        public Button CreateButton;
        public Button ScanButton;
        public Button RemoveButton;
        public SpatialMapGrid SpatialMapGrid;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!SpatialMapGrid)
            {
                SpatialMapGrid = GetComponentInChildren<SpatialMapGrid>(true);
            }
        }

        protected override void Awake()
        {
            base.Awake();
        }

        private void OnEnable()
        {
            StopAllCoroutines();
        }

        public void EnableEdit(bool enable)
        {
            EditButton.interactable = enable;
        }

        public void EnablePreview(bool enable)
        {
            PreviewButton.interactable = enable;
        }
        
        public void EnableScan(bool enable)
        {
            ScanButton.interactable = enable;
        }
        
        public void EnableRemove(bool enable)
        {
            RemoveButton.interactable = enable;
        }

        public void SetData(List<MapMeta> mapMetas, List<SpatialMapData> spatialMaps)
        {
            SpatialMapGrid.SetData(mapMetas, spatialMaps);
        }
    }
}
