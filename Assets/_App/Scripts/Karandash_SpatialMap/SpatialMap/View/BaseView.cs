﻿using System;
using UnityEngine;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class BaseView : ExtendedMonoBehaviour
    {
        public GameObject ProgressPanel;
        public GameObject ErrorPanel;

        protected override void InitComponents()
        {
            base.InitComponents();
        }

        protected virtual void Awake()
        {
            InitComponents();
            
            ProgressPanel.SetActive(false);
            ErrorPanel.SetActive(false);
        }

        public void ShowError(bool isShow)
        {
            if (isShow)
            {
                ErrorPanel.SetActive(true);
            }
            else
            {
                ErrorPanel.SetActive(false);
            }
        }

        public void ShowLoading(bool isShow)
        {
            if (isShow)
            {
                ProgressPanel.SetActive(true);
            }
            else
            {
                ProgressPanel.SetActive(false);
            }
        }
    }
}