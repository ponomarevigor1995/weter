﻿using easyar;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace ZBoom.Common.SpatialMap
{
    public class CreateView : BaseView
    {
        public GameObject UploadPopup;
        public GameObject ProgressBar;
        public TMP_InputField MapNameInput;
        public Button UploadEasyArButton;
        public Button UploadServerDataButton;
        public Button UploadCancelButton;
        public Button SaveButton;
        public Button SnapshotButton;
        public RawImage PreviewImage;

        private DenseSpatialMapBuilderFrameFilter m_DenseSpatialMapBuilderFrameFilter;
        private MapSession m_MapSession;

        private string m_MapName = null;
        private string m_MapId = null;
        private string m_FullPath = null;
        private string m_NameMesh = null;
        private string m_UrlMesh = null;
        private Texture2D m_CapturedImage;

        protected override void InitComponents()
        {
            base.InitComponents();
        }

        protected override void Awake()
        {
            base.Awake();
        }

        private void Start()
        {
        }

        private void OnEnable()
        {
            SaveButton.gameObject.SetActive(true);
            SaveButton.interactable = false;

            StopUploadUI();
            UploadPopup.gameObject.SetActive(false);

            var buttonEasyArText = UploadEasyArButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonEasyArText.text = "Save";

            var buttonServerDataText = UploadServerDataButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonServerDataText.text = "Save";
        }

        private void Update()
        {
            if ((m_MapSession.MapWorker.LocalizedMap != null &&
                 m_MapSession.MapWorker.LocalizedMap.PointCloud.Count >= 20) && !Application.isEditor)
            {
                SaveButton.interactable = true;
            }
            else
            {
                SaveButton.interactable = false;
            }
        }

        private void OnDestroy()
        {
            if (m_CapturedImage)
            {
                Destroy(m_CapturedImage);
            }
        }

        public void SetMapSession(MapSession session)
        {
            m_MapSession = session;
            Clear();
        }

        private void Clear()
        {
            m_MapName = null;
            m_MapId = null;
            m_FullPath = null;
            m_NameMesh = null;
            m_UrlMesh = null;
        }

        public void Save()
        {
            SaveButton.gameObject.SetActive(false);
            UploadPopup.gameObject.SetActive(true);
            //MapNameInput.text = m_MapName = "Map_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm");
            MapNameInput.text = m_MapName = "Map: " + DateHelper.GetCurrentDate();
            m_MapSession.MapWorker.enabled = false;
            Snapshot();
        }

        public void Snapshot()
        {
            var oneShot = Camera.main.gameObject.AddComponent<OneShot>();
            oneShot.Shot(true, (texture) =>
            {
                if (m_CapturedImage)
                {
                    Destroy(m_CapturedImage);
                }

                m_CapturedImage = texture;
                PreviewImage.texture = m_CapturedImage;
            });
        }

        public void OnMapNameChange(string name)
        {
            UploadEasyArButton.interactable = !string.IsNullOrEmpty(name);
            m_MapName = name;
        }

        public void Upload()
        {
            UploadEasyArButton.gameObject.SetActive(true);
            UploadServerDataButton.gameObject.SetActive(false);

            using (var buffer = easyar.Buffer.wrapByteArray(m_CapturedImage.GetRawTextureData()))
            using (var image =
                   new easyar.Image(buffer, PixelFormat.RGB888, m_CapturedImage.width, m_CapturedImage.height))
            {
                m_MapSession.MapWorker.MapUnload += (controller, info, arg3, arg4) =>
                {
                    if (info != null)
                    {
                    }
                };

                m_MapSession.MapWorker.BuilderMapController.MapHost += (map, isSuccessful, error) =>
                {
                    if (isSuccessful)
                    {
                        m_MapId = map.ID;
                    }
                    else
                    {
                    }
                };

                m_MapSession.Save(m_MapName, image);
            }

            StartUploadUI();
            StartCoroutine(Saving());
        }

        private IEnumerator Saving()
        {
            while (m_MapSession.IsSaving)
            {
                yield return 0;
            }

            if (m_MapSession.Saved)
            {
                SaveDataServer();
                //Debug.Log("ViewManager: 1 " + m_MapSession.MapWorker.BuilderMapController.);
                //ViewManager.Instance.LoadEditView(m_MapSession, m_MapSession.MapWorker.BuilderMapController.MapInfoSource.ID);
                //ViewManager.Instance.LoadEditView(m_MapSession, m_MapId);
            }
            else
            {
                StopUploadUI();
                ShowErrorEasyAR();
            }
            //StopUploadUI();
        }

        public void SaveDataServer()
        {
            SpatialMapData spatialMapData = new SpatialMapData();
            spatialMapData.Id = m_MapId;
            spatialMapData.Name = m_MapName;

            string date = DateHelper.GetCurrentDate();
            spatialMapData.CreateAt = date;
            spatialMapData.UpdateAt = date;

            ViewManager.Instance.SaveSpatialMap(spatialMapData, new ResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        StopUploadUI();
                        gameObject.SetActive(false);
                        ViewManager.Instance.LoadMainView();
                    });
                },

                OnError = (errorMessage) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        StopUploadUI();
                        ShowErrorServerData();
                    });
                }
            });
        }

        private void ShowErrorEasyAR()
        {
            UploadEasyArButton.gameObject.SetActive(true);
            UploadServerDataButton.gameObject.SetActive(false);

            var buttonText = UploadEasyArButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = "Try again";
        }

        private void ShowErrorServerData()
        {
            UploadEasyArButton.gameObject.SetActive(false);
            UploadServerDataButton.gameObject.SetActive(true);

            var buttonText = UploadServerDataButton.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = "Try again";
        }

        private void StartUploadUI()
        {
            UploadEasyArButton.interactable = false;
            UploadServerDataButton.interactable = false;
            MapNameInput.interactable = false;
            UploadCancelButton.interactable = true;
            SnapshotButton.interactable = false;
            ProgressBar.SetActive(true);
        }

        private void StopUploadUI()
        {
            UploadEasyArButton.interactable = true;
            UploadServerDataButton.interactable = true;
            MapNameInput.interactable = true;
            UploadCancelButton.interactable = true;
            SnapshotButton.interactable = true;
            ProgressBar.SetActive(false);
        }
    }
}