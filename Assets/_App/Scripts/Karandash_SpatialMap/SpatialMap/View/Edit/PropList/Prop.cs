﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class Prop : ExtendedMonoBehaviour
    {
        public ARPropManager ARPropManager;
        public PropItem PropItemPrefab;

        private List<PropItem> m_PropItems = new List<PropItem>();
        private int m_SelectedPosition = -1;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!ARPropManager)
            {
                ARPropManager = FindObjectOfType<ARPropManager>(true);
            }
        }

        private void Awake()
        {
            InitComponents();
        }

        private void Start()
        {
            foreach (PropCollection.Template template in PropCollection.Instance.Templates)
            {
                PropItem propItem = Instantiate(PropItemPrefab, transform);
                propItem.SetData(template);
                m_PropItems.Add(propItem);
            }
        }

        public void Select()
        {
            PropItem propItem = m_PropItems[0];
            
            if (m_SelectedPosition != -1)
            {
                m_PropItems[m_SelectedPosition].Deselect();
                if (propItem == m_PropItems[m_SelectedPosition])
                {
                    m_SelectedPosition = -1;
                }
                else
                {
                    m_SelectedPosition = 0;
                    ARPropManager.SelectTemplate(propItem);
                }
            }
            else
            {
                m_SelectedPosition = 0;
                ARPropManager.SelectTemplate(propItem);
            }
        }

        public void Deselect(PropItem propItemController = null)
        {
            if (m_SelectedPosition != -1)
            {
                m_PropItems[m_SelectedPosition].Deselect();
            }
            
            m_SelectedPosition = -1;
            //ARPropManager.DeselectTemplate();
        }

        public void DeselectAll()
        {
            foreach (PropItem propItem in m_PropItems)
            {
                propItem.Deselect();
            }

            m_SelectedPosition = -1;
        }
    }
}