﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ZBoom.Common.SpatialMap
{
    public class PropGroupItem : MonoBehaviour
    {
        [SerializeField] public TextMeshProUGUI Title;
        [SerializeField] public GameObject Grid;
    }
}