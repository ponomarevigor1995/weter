﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class PrefabGrid : ExtendedMonoBehaviour
    {
        public ARPropManager ARPropManager;
        public PropGroupItem PropGroupItemPrefab;
        public PropItem PropItemPrefab;

        private List<PropItem> m_PropItems = new List<PropItem>();
        private int m_SelectedPosition = -1;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (ARPropManager)
            {
                ARPropManager = FindObjectOfType<ARPropManager>(true);
            }
        }

        private void Awake()
        {
            InitComponents();
        }

        private void Start()
        {
            /*
            foreach (var templet in PropCollection.Instance.Templates)
            {
                var scanPropCellController = Instantiate(PropItemPrefab, transform);
                scanPropCellController.SetMapMeta(templet);
                m_PropItems.Add(scanPropCellController);
            }
            */
            
            foreach (PropCollection.TemplateGroup group in PropCollection.Instance.TemplateGroups)
            {
                PropGroupItem propGroupItemPrefab = Instantiate(PropGroupItemPrefab, transform);
                propGroupItemPrefab.Title.text = group.Name;
                
                foreach (PropCollection.Template template in group.Templates)
                {
                    PropItem propItem = Instantiate(PropItemPrefab, propGroupItemPrefab.Grid.transform);
                    propItem.SetData(template);
                    m_PropItems.Add(propItem);
                }
            }
        }

        public void Select(PropItem propItem)
        {
            if (m_SelectedPosition != -1)
            {
                m_PropItems[m_SelectedPosition].Deselect();
                if (propItem == m_PropItems[m_SelectedPosition])
                {
                    m_SelectedPosition = -1;
                }
                else
                {
                    m_SelectedPosition = m_PropItems.IndexOf(propItem);
                    ARPropManager.SelectTemplate(propItem);
                }
            }
            else
            {
                m_SelectedPosition = m_PropItems.IndexOf(propItem);
                ARPropManager.SelectTemplate(propItem);
            }
        }

        public void Deselect(PropItem propItem = null)
        {
            if (m_SelectedPosition != -1)
            {
                m_PropItems[m_SelectedPosition].Deselect();
            }
            
            m_SelectedPosition = -1;
            
            /*
            if (propItem != null)
            {
                propItem.Deselect();
            }
            */
            
            //ARPropManager.DeselectTemplate();
        }

        public void DeselectAll()
        {
            foreach (PropItem propItem in m_PropItems)
            {
                propItem.Deselect();
            }

            m_SelectedPosition = -1;
        }
    }
}