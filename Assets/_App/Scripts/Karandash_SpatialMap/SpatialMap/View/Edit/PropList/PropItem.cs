﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class PropItem : ExtendedMonoBehaviour
    {
        [HideInInspector] public PropGrid PropGrid;
        [HideInInspector] public PrefabGrid PrefabGrid;
        public Image Icon;
        public Image Background;
        public TextMeshProUGUI NameText;
        [HideInInspector] public bool IsSelected = false;

        public PropCollection.Template Template { get; private set; }
        public event Action SelectEvent;
        public event Action DeselectEvent;

        [SerializeField] private Color m_DefaultColor;
        [SerializeField] private Color m_SelectedColor;
        [SerializeField] private float m_DefaultScale = 1.0f;
        [SerializeField] private float m_SelectedScale = 1.05f;


        protected override void InitComponents()
        {
            base.InitComponents();
            if (!PropGrid)
            {
                PropGrid = GetComponentInParent<PropGrid>();
            }

            if (!PrefabGrid)
            {
                PrefabGrid = GetComponentInParent<PrefabGrid>();
            }
        }

        private void Awake()
        {
            InitComponents();
            Deselect();
        }

        public void SetData(PropCollection.Template template)
        {
            Template = template;
            Icon.sprite = template.Icon;
            if (NameText != null)
            {
                NameText.text = template.Name;
            }
        }

        public void SwitchSelected()
        {
            IsSelected = !IsSelected;

            if (IsSelected)
            {
                Select();
            }
            else
            {
                Deselect();
            }
        }

        public void Select()
        {
            if (!IsSelected)
            {
                IsSelected = true;

                Background.color = m_SelectedColor;
                //NameText.fontStyle = FontStyles.Bold;
                Background.transform.localScale = new Vector3(m_SelectedScale, m_SelectedScale, m_SelectedScale);

                if (PropGrid != null)
                {
                    PropGrid.Select(this);
                }

                if (PrefabGrid != null)
                {
                    PrefabGrid.Select(this);
                }

                if (SelectEvent != null)
                {
                    SelectEvent();
                }
            }
        }

        public void Deselect()
        {
            if (IsSelected)
            {
                IsSelected = false;

                Background.color = m_DefaultColor;
                //NameText.fontStyle = FontStyles.Normal;
                Background.transform.localScale = new Vector3(m_DefaultScale, m_DefaultScale, m_DefaultScale);

                if (DeselectEvent != null)
                {
                    DeselectEvent();
                }
            }
        }
    }
}