﻿using System;
using System.Collections;
using System.Collections.Generic;
using easyar;
using Michsky.UI.ModernUIPack;
using RTG;
using UnityEngine;
using UnityEngine.EventSystems;
using Weter;
using TouchController = Weter.TouchController;

namespace ZBoom.Common.SpatialMap
{
    public class ARPropManager : ExtendedMonoBehaviour
    {
        public GameObject PropCollectionPanel;
        public PrefabGrid PrefabGrid;

        public GameObject OutlinePrefab;

        public SwitchManager FreeMoveSwitch;
        public SwitchManager CloudPointSwitch;
        public SwitchManager GizmoSwitch;

        public GameObject AddPanel;
        public GameObject TapPanel;
        public GameObject EditPanel;
        public GameObject GizmoPanel;
        public GameObject RotateScalePanel;

        public SliderManager RotateSlider;
        public SliderManager ScaleSlider;

        public GameObject StopEditButton;
        public GameObject DeleteButton;

        private PropItem m_SelectedPropItem;

        [SerializeField] private TouchController m_TouchController;
        [SerializeField] private GizmoController m_GizmoController;
        private MapSession m_MapSession;
        private GameObject m_SelectedGameObject;
        private Camera m_Camera;
        private bool m_IsMoveFree = true;
        private bool m_UseGizmo = false;
        [SerializeField] private bool m_UseCenterPosition = true;
        [SerializeField] private bool m_UseAutoScale = true;
        [SerializeField] private float m_ScaleStep = 0.25f;

        public event Action<GameObject> CreateObjectEvent;
        public event Action<GameObject> DeleteObjectEvent;

        private List<GameObject> m_ArObjects = new List<GameObject>();

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!PrefabGrid)
            {
                PrefabGrid = FindObjectOfType<PrefabGrid>(true);
            }

            if (!m_TouchController)
            {
                m_TouchController = GetComponentInChildren<TouchController>(true);
            }

            if (!m_GizmoController)
            {
                m_GizmoController = GetComponentInChildren<GizmoController>(true);
            }
        }

        private void Awake()
        {
            InitComponents();

            m_Camera = Camera.main;
            m_SelectedPropItem = null;

            m_TouchController.OnScale += localScale =>
            {
                if (m_SelectedGameObject != null)
                {
                    TowerFlow towerFlow = m_SelectedGameObject.GetComponent<TowerFlow>();
                    if (towerFlow == null)
                    {
                        towerFlow = m_SelectedGameObject.GetComponentInChildren<TowerFlow>();
                    }

                    if (towerFlow != null)
                    {
                        towerFlow.UpdateScale(localScale.x);
                    }
                }
            };

            OutlinePrefab = Instantiate(OutlinePrefab);
            OutlinePrefab.SetActive(false);

            StopEditButton.SetActive(false);
            DeleteButton.SetActive(false);

            AddPanel.SetActive(true);
            TapPanel.SetActive(false);
            EditPanel.SetActive(false);

            RotateScalePanel.SetActive(false);
            GizmoPanel.SetActive(false);

            CloudPointSwitch.isOn = true;
            FreeMoveSwitch.isOn = true;
            GizmoSwitch.isOn = false;

            m_IsMoveFree = FreeMoveSwitch.isOn;
            m_UseGizmo = GizmoSwitch.isOn;

            if (m_UseGizmo)
            {
                m_TouchController.enabled = false;
                m_GizmoController.enabled = true;
            }
            else
            {
                m_TouchController.enabled = true;
                m_GizmoController.enabled = false;
            }
        }

        private void Update()
        {
            Vector3? checkTouch = TouchHelper.CheckTouch();
            if (checkTouch == null)
            {
                return;
            }

            if (m_MapSession == null)
            {
                return;
            }
            
            Vector3 inputPosition = (Vector3)checkTouch;

            if (m_SelectedPropItem != null)
            {
                Optional<Vector3> point = m_MapSession.HitTestOne(
                    new Vector2(
                        inputPosition.x / Screen.width,
                        inputPosition.y / Screen.height));

                if (point.OnSome)
                {
                    StopEditGameObject();
                    CreateGameObject(m_SelectedPropItem, point.Value);
                    StartEditGameObject(m_SelectedGameObject);
                }
                else
                {
                    Ray ray = m_Camera.ScreenPointToRay(inputPosition);
                    RaycastHit hitInfo;
                    if (Physics.Raycast(ray, out hitInfo))
                    {
                        if (hitInfo.transform.GetComponent<DenseSpatialMapBlockController>())
                        {
                            StopEditGameObject();
                            CreateGameObject(m_SelectedPropItem, hitInfo.point);
                            StartEditGameObject(m_SelectedGameObject);
                        }
                        /*
                        if (hitInfo.transform.GetComponent<ShadowPlane>())
                        {
                            StopEditGameObject();
                            CreateGameObject(m_SelectedPropItem, hitInfo.point);
                            StartEditGameObject(m_SelectedGameObject);
                        }
                        */
                    }
                }
            }

            if (m_SelectedGameObject == null)
            {
                Ray ray = m_Camera.ScreenPointToRay(inputPosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    StopEditGameObject();
                    StartEditGameObject(hitInfo.collider.gameObject);
                }
            }
            else
            {
                if (!m_IsMoveFree)
                {
                    Optional<Vector3> point = m_MapSession.HitTestOne(
                        new Vector2(
                            inputPosition.x / Screen.width,
                            inputPosition.y / Screen.height));
                    if (point.OnSome)
                    {
                        if (m_UseCenterPosition)
                        {
                            m_SelectedGameObject.transform.position = point.Value;
                        }
                        else
                        {
                            m_SelectedGameObject.transform.position = point.Value + Vector3.up * m_SelectedGameObject.transform
                                                                          .localScale.y / 2;
                        }
                    }
                }
            }
        }

        private void OnDisable()
        {
            m_MapSession = null;
            StopEditGameObject();
        }

        public void SetMapSession(MapSession session)
        {
            m_MapSession = session;
            if (m_MapSession.MapWorker)
            {
                m_MapSession.MapWorker.MapLoad += (arg1, arg2, arg3, arg4) =>
                {
                    
                };
            }
        }

        public void SetFreeMove(bool isFree)
        {
            m_IsMoveFree = isFree;
            if (m_SelectedGameObject)
            {
                if (isFree)
                {
                    m_TouchController.TurnOn(m_SelectedGameObject.transform, m_Camera, true, true, true, true);
                }
                else
                {
                    m_TouchController.TurnOn(m_SelectedGameObject.transform, m_Camera, false, false, true, true);
                }
            }
        }

        public void SelectTemplate(PropItem propItemController)
        {
            m_SelectedPropItem = propItemController;
            SelectGameObject();
        }

        private void SelectGameObject()
        {
            StopEditGameObject();

            RotateScalePanel.SetActive(false);
            GizmoPanel.SetActive(false);

            PropCollectionPanel.SetActive(false);

            AddPanel.SetActive(false);
            TapPanel.SetActive(true);
            EditPanel.SetActive(false);
        }

        private void CreateGameObject(PropItem propItem, Vector3 initPosition)
        {
            m_SelectedGameObject = Instantiate(propItem.Template.Object);
            m_SelectedGameObject.name = propItem.Template.Object.name;

            Create(m_SelectedGameObject, initPosition);
        }

        private void Create(GameObject selectedGameObject, Vector3 initPosition)
        {
            float localScale = 1f;
            if (m_UseAutoScale)
            {
                float distance = Vector3.Distance(m_Camera.transform.position, initPosition);
                localScale = distance * m_ScaleStep;
                if (localScale >= 1)
                {
                    localScale = 1f;
                }
            }

            selectedGameObject.transform.localScale = new Vector3(localScale, localScale, localScale);

            if (m_UseCenterPosition)
            {
                selectedGameObject.transform.position = initPosition;
            }
            else
            {
                selectedGameObject.transform.position =
                    initPosition + Vector3.up * selectedGameObject.transform.localScale.y / 2;
            }

            if (CreateObjectEvent != null)
            {
                CreateObjectEvent(selectedGameObject);
            }

            ObjectAR objectAR = selectedGameObject.GetComponent<ObjectAR>();
            if (objectAR != null)
            {
                objectAR.SetState(ObjectAR.State.EDIT);
            }

            FreeMoveSwitch.gameObject.SetActive(true);
            DeselectTemplate();
        }

        public void DeselectTemplate()
        {
            if (PrefabGrid != null)
            {
                PrefabGrid.Deselect();
            }

            m_SelectedPropItem = null;

            AddPanel.SetActive(true);
            TapPanel.SetActive(false);
            EditPanel.SetActive(false);
            RotateScalePanel.SetActive(false);
            GizmoPanel.SetActive(false);
        }

        private void StartEditGameObject(GameObject editGameObject)
        {
            if (editGameObject.transform.GetComponent<ObjectAR>() == null)
            {
                return;
            }

            DeselectTemplate();

            if (!GizmoSwitch.isOn)
            {
                RotateScalePanel.SetActive(true);
            }
            else
            {
                GizmoPanel.SetActive(true);
            }

            RotateSlider.mainSlider.value = editGameObject.transform.localRotation.eulerAngles.y;
            //RotateSlider.UpdateUI();
            ScaleSlider.mainSlider.value = editGameObject.transform.localScale.x;
            //ScaleSlider.UpdateUI();

            AddPanel.SetActive(false);
            TapPanel.SetActive(false);
            EditPanel.SetActive(true);

            StopEditButton.SetActive(true);
            DeleteButton.SetActive(true);

            m_SelectedGameObject = editGameObject;

            MeshFilter meshFilter = m_SelectedGameObject.GetComponentInChildren<MeshFilter>();
            BoxCollider boxCollider = m_SelectedGameObject.GetComponent<BoxCollider>();

            if (boxCollider != null)
            {
                OutlinePrefab.SetActive(true);
                //OutlinePrefab.GetComponent<MeshFilter>().mesh = meshFilter.mesh;
                OutlinePrefab.transform.parent = m_SelectedGameObject.transform;
                //OutlinePrefab.transform.localPosition = Vector3.zero;
                OutlinePrefab.transform.localPosition = boxCollider.center;
                OutlinePrefab.transform.localRotation = Quaternion.identity;
                OutlinePrefab.transform.localScale = boxCollider.size;
            }
            else
            {
                return;
            }

            if (!m_UseGizmo)
            {
                m_TouchController.TurnOn(m_SelectedGameObject.transform, m_Camera, true, true, true, true);
            }
            else
            {
                m_GizmoController.OnTargetObjectChanged(m_SelectedGameObject);
            }

            SetFreeMove(m_IsMoveFree);
        }

        public void StopEditGameObject()
        {
            RotateScalePanel.SetActive(false);
            GizmoPanel.SetActive(false);

            AddPanel.SetActive(true);
            TapPanel.SetActive(false);
            EditPanel.SetActive(false);

            m_SelectedGameObject = null;

            if (OutlinePrefab)
            {
                OutlinePrefab.transform.parent = null;
                OutlinePrefab.SetActive(false);
            }

            if (!m_UseGizmo)
            {
                if (m_TouchController != null)
                {
                    m_TouchController.TurnOff();
                }
            }
            else
            {
                m_GizmoController.OnTargetObjectChanged(null);
            }
        }

        public void DeleteSelection()
        {
            if (!m_SelectedGameObject)
            {
                return;
            }

            if (DeleteObjectEvent != null)
            {
                DeleteObjectEvent(m_SelectedGameObject);
            }

            Destroy(m_SelectedGameObject);
            StopEditGameObject();
        }

        public void CopySelection()
        {
            if (m_SelectedGameObject != null)
            {
                if (OutlinePrefab)
                {
                    OutlinePrefab.transform.parent = null;
                    OutlinePrefab.SetActive(false);
                }

                var copySelectedGameObject = Instantiate(m_SelectedGameObject);
                copySelectedGameObject.transform.parent = m_SelectedGameObject.transform.parent;
                copySelectedGameObject.name = m_SelectedGameObject.name;

                copySelectedGameObject.transform.position = m_SelectedGameObject.transform.position;
                copySelectedGameObject.transform.rotation = m_SelectedGameObject.transform.rotation;
                copySelectedGameObject.transform.localScale = m_SelectedGameObject.transform.localScale;

                if (CreateObjectEvent != null)
                {
                    CreateObjectEvent(copySelectedGameObject);
                }

                m_ArObjects.Add(copySelectedGameObject);

                StopEditGameObject();
                StartEditGameObject(copySelectedGameObject);
            }
        }

        public void UseGizmo(bool useGizmo)
        {
            StopEditGameObject();

            m_UseGizmo = useGizmo;
            if (!useGizmo)
            {
                m_GizmoController.enabled = false;
                m_TouchController.enabled = true;
            }
            else
            {
                m_GizmoController.enabled = true;
                m_TouchController.enabled = false;
            }
        }

        public void Rotate(float rotation)
        {
            if (m_SelectedGameObject != null)
            {
                m_SelectedGameObject.transform.localRotation = Quaternion.Euler(0f, rotation, 0f);
            }
        }

        public void Scale(float scale)
        {
            if (m_SelectedGameObject != null)
            {
                m_SelectedGameObject.transform.localScale = new Vector3(scale, scale, scale);
            }
        }
    }
}