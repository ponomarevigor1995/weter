﻿using System;
using Michsky.UI.ModernUIPack;
using System.Collections.Generic;
using easyar;

using UnityEngine;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class EditView : BaseView
    {
        public ARPropManager ARPropManager;
        public Material MeshMaterial;

        private MapSession m_MapSession;
        private MapSession.MapData m_MapData;
        private SpatialMapData m_SpatialMapData;
        private SparseSpatialMapController m_SparseSpatialMapController;
        private DenseSpatialMapBuilderFrameFilter m_DenseSpatialMapBuilderFrameFilter;
        [SerializeField] private bool m_UseSpatialDenseMap = false;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!ARPropManager)
            {
                ARPropManager = GetComponentInChildren<ARPropManager>();
            }
        }

        protected override void Awake()
        {
            base.Awake();
            ARPropManager.CreateObjectEvent += (gameObj) =>
            {
                if (gameObj)
                {
                    gameObj.transform.parent = m_MapData.Controller.transform;
                    m_MapData.Props.Add(gameObj);
                }
            };
            ARPropManager.DeleteObjectEvent += (gameObj) =>
            {
                if (gameObj)
                {
                    m_MapData.Props.Remove(gameObj);
                }
            };
        }

        private void OnEnable()
        {
            UseDenseSpatialMap(m_UseSpatialDenseMap);
        }

        public void SetMapSession(MapSession session)
        {
            m_MapData = session.Maps[0];
            m_MapSession = session;
            ARPropManager.SetMapSession(session);

            m_DenseSpatialMapBuilderFrameFilter =
                m_MapSession.ARSession.GetComponentInChildren<DenseSpatialMapBuilderFrameFilter>(true);
            if (m_DenseSpatialMapBuilderFrameFilter != null)
            {
                m_DenseSpatialMapBuilderFrameFilter.gameObject.SetActive(true);
                m_DenseSpatialMapBuilderFrameFilter.RenderMesh = true;

                UseDenseSpatialMap(m_UseSpatialDenseMap);
            }
        }

        public void SetSpatialMap(SpatialMapData spatialMapData, SparseSpatialMapController sparseSpatialMapController)
        {
            m_SpatialMapData = spatialMapData;
            m_SparseSpatialMapController = sparseSpatialMapController;
        }

        public void Save(bool isSaveToMetaManager = false)
        {
            if (m_MapData == null)
            {
                return;
            }

            ShowLoading(true);
            ShowError(false);

            List<MapMeta.PropInfo> propInfos = new List<MapMeta.PropInfo>();
            SpatialMapData spatialMapData = new SpatialMapData()
            {
                Id = m_MapData.Meta.Map.ID,
                Name = m_MapData.Meta.Map.Name
            };

            foreach (var prop in m_MapData.Props)
            {
                var propName = prop.name;
                var position = prop.transform.localPosition;
                var rotation = prop.transform.localRotation;
                var eulerRotation = prop.transform.localRotation.eulerAngles;
                var scale = prop.transform.localScale;

                propInfos.Add(new MapMeta.PropInfo()
                {
                    Name = propName,
                    Position = new float[3] { position.x, position.y, position.z },
                    Rotation = new float[4] { rotation.x, rotation.y, rotation.z, rotation.w },
                    Scale = new float[3] { scale.x, scale.y, scale.z }
                });

                SpatialMapItemData spatialMapItemData = new SpatialMapItemData()
                {
                    Id = propName,
                    Name = propName,
                    Position = new float[3] { position.x, position.y, position.z },
                    Rotation = new float[4] { rotation.x, rotation.y, rotation.z, rotation.w },
                    EulerRotation = new float[3] { eulerRotation.x, eulerRotation.y, eulerRotation.z },
                    Scale = new float[3] { scale.x, scale.y, scale.z }
                };

                TowerSettings towerSettingsController =
                    prop.transform.GetComponent<TowerSettings>();
                if (towerSettingsController != null)
                {
                    spatialMapItemData.EnabledFlow = (int)towerSettingsController.CurrentFlowType;
                    spatialMapItemData.EnabledHex = (int)towerSettingsController.CurrentHexType;
                    spatialMapItemData.EnabledShield = (int)towerSettingsController.CurrentShieldType;
                    spatialMapItemData.EnabledTower = (int)towerSettingsController.CurrentTowerType;
                    spatialMapItemData.EnabledUI = (int)towerSettingsController.CurrentUiType;
                }

                spatialMapData.MapItems.Add(spatialMapItemData);
            }

            m_MapData.Meta.Props = propInfos;
            if (isSaveToMetaManager)
            {
                MapMetaManager.Save(m_MapData.Meta);
            }

            string date = DateHelper.GetCurrentDate();
            spatialMapData.UpdateAt = date;

            ViewManager.Instance.SaveSpatialMap(spatialMapData, new ResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        ShowLoading(false);
                        ShowError(false);

                        gameObject.SetActive(false);
                        ViewManager.Instance.LoadMainView();
                    });
                },

                OnError = (errorMessage) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        ShowLoading(false);
                        ShowError(true);

                        //ShowError();
                    });
                }
            });

            //ViewManager.Instance.SaveSpatialMap(spatialMapData);
        }

        public void UseDenseSpatialMap(bool useDenseSpatialMap)
        {
            m_UseSpatialDenseMap = useDenseSpatialMap;

            if (m_DenseSpatialMapBuilderFrameFilter)
            {
                if (useDenseSpatialMap)
                {
                    m_DenseSpatialMapBuilderFrameFilter.RenderMesh = true;
                    m_DenseSpatialMapBuilderFrameFilter.UseCollider = true;
                }
                else
                {
                    m_DenseSpatialMapBuilderFrameFilter.RenderMesh = false;
                    m_DenseSpatialMapBuilderFrameFilter.UseCollider = false;
                }
            }
        }
    }
}