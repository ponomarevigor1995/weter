﻿using System;
using Michsky.UI.ModernUIPack;
using System.Collections.Generic;
using easyar;

using UnityEngine;
using Weter;

namespace ZBoom.Common.SpatialMap
{
    public class PreviewView : BaseView
    {
        public SwitchManager MeshOcclusionSwitch;
        public SwitchManager PrimitiveOcclusionSwitch;
        public Material MeshMaterial;

        private MapSession m_MapSession;
        private List<SpatialMapData> m_SpatialMaps = new List<SpatialMapData>();
        private List<SparseSpatialMapController> m_SparseSpatialMaps = new List<SparseSpatialMapController>();

        protected override void Awake()
        {
            base.Awake();
        }

        private void OnEnable()
        {
        }

        public void SetMapSession(MapSession session)
        {
            m_MapSession = session;
            m_SpatialMaps.Clear();
            m_SparseSpatialMaps.Clear();
        }

        public void SetSpatialMaps(List<SpatialMapData> spatialMapData, List<SparseSpatialMapController> sparseSpatialMapController)
        {
            m_SpatialMaps = spatialMapData;
        }

        public void LocalizeSpatialMap(SpatialMapData spatialMapData, SparseSpatialMapController sparseSpatialMapController)
        {
            if (!m_SpatialMaps.Exists(data => data.Id.Equals(spatialMapData.Id)))
            {
                m_SparseSpatialMaps.Add(sparseSpatialMapController);
                m_SpatialMaps.Add(spatialMapData);
                UsePrimitiveOcclusion(MeshOcclusionSwitch.isOn);
            }
        }

        public void UsePrimitiveOcclusion(bool useOcclusion)
        {
            var primitives = FindObjectsOfType<OcclusionFigureController>(true);
            foreach (var primitiveMesh in primitives)
            {
                if (useOcclusion)
                {
                    primitiveMesh.gameObject.SetActive(true);
                }
                else
                {
                    primitiveMesh.gameObject.SetActive(false);
                }
            }
        }
    }
}