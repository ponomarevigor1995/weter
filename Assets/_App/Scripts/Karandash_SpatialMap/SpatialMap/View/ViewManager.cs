﻿using easyar;
using System;
using System.Collections;
using System.Collections.Generic;
using _App.Scripts.Karandash_SpatialMap.SpatialMap;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;
using Weter;
using Weter.Slam;

namespace ZBoom.Common.SpatialMap
{
    public class ViewManager : ExtendedMonoBehaviour
    {
        public static ViewManager Instance;

        public SpatialMapGameSession GameSession;
        public SpatialMapFirebaseRepository RealtimeFirebaseController;
        public LocalRepository LocalRepository;
        public SparseSpatialMapController SparseSpatialMapPrefab;
        public MapMenuView MapMenuView;
        public CreateView CreateView;
        public EditView EditView;
        public PreviewView Preview;
        public SceneManager SceneManager;
        public Text Status;

        [SerializeField] private bool m_IsShowStatus = false;

        [SerializeField] private ARSession m_ARSessionPrefab;
        private ARSession m_ARSession;
        private MapSession m_MapSession;
        private SparseSpatialMapWorkerFrameFilter m_MapFrameFilter;
        private List<MapMeta> m_SelectedMaps = new List<MapMeta>();
        private List<SpatialMapData> m_SpatialMaps = new List<SpatialMapData>();
        private string m_DeviceModel = string.Empty;

        private bool m_IsAdminMode = false;

        #if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
        static void ImportSampleStreamingAssets()
        {
            FileUtil.ImportSampleStreamingAssets();
        }
        #endif

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!m_ARSessionPrefab)
            {
                m_ARSessionPrefab = FindObjectOfType<ARSession>(true);
            }

            if (!GameSession)
            {
                GameSession = FindObjectOfType<SpatialMapGameSession>();
            }

            if (!RealtimeFirebaseController)
            {
                RealtimeFirebaseController = FindObjectOfType<SpatialMapFirebaseRepository>();
            }

            if (!LocalRepository)
            {
                LocalRepository = FindObjectOfType<LocalRepository>();
            }

            if (!MapMenuView)
            {
                MapMenuView = FindObjectOfType<MapMenuView>(true);
            }

            if (!CreateView)
            {
                CreateView = FindObjectOfType<CreateView>(true);
            }

            if (!Preview)
            {
                Preview = FindObjectOfType<PreviewView>(true);
            }

            if (!SceneManager)
            {
                SceneManager = FindObjectOfType<SceneManager>();
            }
        }

        private void Awake()
        {
            InitComponents();
            Instance = this;
            MapMenuView.gameObject.SetActive(false);
            CreateView.gameObject.SetActive(false);
            EditView.gameObject.SetActive(false);
            Preview.gameObject.SetActive(false);
        }

        private void Start()
        {
            m_IsAdminMode = LocalRepository.IsAdminMode();
            if (m_IsAdminMode)
            {
                LoadMainView();
            }
            else
            {
                Preview.gameObject.SetActive(true);
                GetSpatialMaps(() =>
                               {
                                   Preview.ShowError(false);
                                   Preview.ShowLoading(true);
                               },
                               () =>
                               {
                                   Preview.ShowLoading(false);
                                   Preview.ShowError(true);
                               },
                               (mapMetas, spatialMaps) =>
                               {
                                   Preview.ShowLoading(false);
                                   Preview.ShowError(false);

                                   m_SelectedMaps.AddRange(mapMetas);
                                   LoadPreviewView();
                               });
            }
        }

        private void Update()
        {
            if (m_ARSession)
            {
                ShowDebugMessage();
            }
        }

        private void ShowDebugMessage()
        {
            if (m_IsShowStatus)
            {
                Status.text = $"Device Model: {SystemInfo.deviceModel} {m_DeviceModel}" + Environment.NewLine +
                              "VIO Device" + Environment.NewLine +
                              "\tType: " +
                              ((m_ARSession.Assembly != null && m_ARSession.Assembly.FrameSource)
                                  ? m_ARSession.Assembly.FrameSource.GetType().ToString().Replace("easyar.", "")
                                               .Replace("FrameSource", "")
                                  : "-") + Environment.NewLine +
                              "\tTracking Status: " + m_ARSession.TrackingStatus + Environment.NewLine +
                              "CenterMode: " + m_ARSession.CenterMode + Environment.NewLine +
                              "Sparse Spatial Map" + Environment.NewLine +
                              "\tWorking Mode: " + m_MapFrameFilter.WorkingMode + Environment.NewLine +
                              "\tLocalization Mode: " + m_MapFrameFilter.LocalizerConfig.LocalizationMode +
                              Environment.NewLine +
                              "Localized Map" + Environment.NewLine +
                              "\tName: " +
                              (m_MapFrameFilter.LocalizedMap == null
                                  ? "-"
                                  : (m_MapFrameFilter.LocalizedMap.MapInfo == null
                                      ? "-"
                                      : m_MapFrameFilter.LocalizedMap.MapInfo.Name)) + Environment.NewLine +
                              "\tID: " + (m_MapFrameFilter.LocalizedMap == null
                                  ? "-"
                                  : (m_MapFrameFilter.LocalizedMap.MapInfo == null
                                      ? "-"
                                      : m_MapFrameFilter.LocalizedMap.MapInfo.ID)) + Environment.NewLine +
                              "\tPoint Cloud Count: " + (m_MapFrameFilter.LocalizedMap == null
                                  ? "-"
                                  : m_MapFrameFilter.LocalizedMap.PointCloud.Count.ToString());
            }
        }

        private void OnDestroy()
        {
            DestroySession();
        }

        public void SelectMaps(List<MapMeta> metas)
        {
            m_SelectedMaps = metas;
            MapMenuView.EnablePreview(m_SelectedMaps.Count > 0);
            MapMenuView.EnableEdit(m_SelectedMaps.Count == 1);
            MapMenuView.EnableScan(m_SelectedMaps.Count == 1);
            MapMenuView.EnableRemove(m_SelectedMaps.Count > 0);
        }

        #region MenuView

        public void LoadMainView(bool isLoadData = true)
        {
            DestroySession();
            SelectMaps(new List<MapMeta>());
            MapMenuView.gameObject.SetActive(true);
            if (isLoadData)
            {
                //GetSpatialMaps();
                UpdateMainView();
            }
        }

        public void UpdateMainView()
        {
            GetSpatialMaps(() =>
                           {
                               MapMenuView.ShowError(false);
                               MapMenuView.ShowLoading(true);
                           },
                           () =>
                           {
                               MapMenuView.ShowLoading(false);
                               MapMenuView.ShowError(true);
                           },
                           (mapMetas, spatialMaps) =>
                           {
                               MapMenuView.SetData(mapMetas, spatialMaps);
                               MapMenuView.ShowLoading(false);
                               MapMenuView.ShowError(false);
                           });
        }

        public void BackFromMainView()
        {
        }

        #endregion

        #region Create View

        public void LoadCreateView()
        {
            CreateSession();
            m_MapSession.SetupMapBuilder(SparseSpatialMapPrefab);
            CreateView.SetMapSession(m_MapSession);
            CreateView.gameObject.SetActive(true);
        }

        public void BackFromCreateView()
        {
            if (m_IsAdminMode)
            {
                CreateView.gameObject.SetActive(false);
                LoadMainView();
            }
            else
            {
                Back();
            }
        }

        #endregion


        #region Ediv View

        public void LoadEditView()
        {
            var spatialMapData = m_SpatialMaps.Find(data => data.Id.Equals(m_SelectedMaps[0].Map.ID));
            if (spatialMapData != null)
            {
                CreateSession();
                m_MapSession.LoadMapMeta(
                    SparseSpatialMapPrefab,
                    true,
                    onObjectCreated: (sparseSpatialMapController, createdObject, propInfo) =>
                    {
                        ObjectAR objectAR = createdObject.GetComponent<ObjectAR>();
                        if (objectAR != null)
                        {
                            objectAR.SetState(ObjectAR.State.EDIT);
                        }

                        UpdateTowerSettings(createdObject, propInfo);
                    },
                    onMapLoaded: (sparseSpatialMapController, info) =>
                    {
                        EditView.SetSpatialMap(spatialMapData, sparseSpatialMapController);
                    },
                    onMapLocalized: (sparseSpatialMapController) => { }
                );

                EditView.SetMapSession(m_MapSession);
                EditView.gameObject.SetActive(true);
            }
        }

        public void BackFromEditView()
        {
            if (m_IsAdminMode)
            {
                EditView.gameObject.SetActive(false);
                LoadMainView();
            }
            else
            {
                Back();
            }
        }

        #endregion

        #region Preview

        public void LoadPreviewView()
        {
            GameSession.Clear();

            CreateSession();
            m_MapSession.LoadMapMeta(
                SparseSpatialMapPrefab,
                false,
                onObjectCreated: (sparseSpatialMapController, createdObject, propInfo) =>
                {
                    ObjectAR objectAR = createdObject.GetComponent<ObjectAR>();
                    if (objectAR != null)
                    {
                        objectAR.SetState(ObjectAR.State.PREVIEW);
                    }

                    UpdateTowerSettings(createdObject, propInfo);
                },
                onMapLoaded: (sparseSpatialMapController, info) => { },
                onMapLocalized: (sparseSpatialMapController) => { GameSession.Activate(); }
            );
            Preview.SetMapSession(m_MapSession);
            Preview.gameObject.SetActive(true);
        }

        public void UpdatePreview()
        {
            GetSpatialMaps(() =>
                           {
                               MapMenuView.ShowError(false);
                               MapMenuView.ShowLoading(true);
                           },
                           () =>
                           {
                               MapMenuView.ShowLoading(false);
                               MapMenuView.ShowError(true);
                           },
                           (mapMetas, spatialMaps) =>
                           {
                               MapMenuView.SetData(mapMetas, spatialMaps);
                               MapMenuView.ShowLoading(false);
                               MapMenuView.ShowError(false);
                           });
        }

        public void BackFromPreview()
        {
            if (m_IsAdminMode)
            {
                Preview.gameObject.SetActive(false);
                LoadMainView();
            }
            else
            {
                Back();
            }
        }

        #endregion

        public void Back()
        {
            SceneManager.LoadScene("Menu");
        }

        private void UpdateTowerSettings(GameObject prop, MapMeta.PropInfo propInfo)
        {
            TowerSettings towerSettings = prop.GetComponent<TowerSettings>();
            if (towerSettings != null)
            {
                SpatialMapItemData spatialMapItemData = new SpatialMapItemData()
                {
                    Id = propInfo.Name,
                    Name = propInfo.Name,
                    Position = new float[3]
                    {
                        prop.transform.localPosition.x,
                        prop.transform.localPosition.y,
                        prop.transform.localPosition.z
                    },
                    Rotation = new float[4]
                    {
                        prop.transform.localRotation.x,
                        prop.transform.localRotation.y,
                        prop.transform.localRotation.z,
                        prop.transform.localRotation.w
                    },
                    EulerRotation = new float[3]
                    {
                        prop.transform.localRotation.eulerAngles.x,
                        prop.transform.localRotation.eulerAngles.y,
                        prop.transform.localRotation.eulerAngles.z
                    },
                    Scale = new float[3]
                    {
                        prop.transform.localScale.x,
                        prop.transform.localScale.y,
                        prop.transform.localScale.z
                    },

                    EnabledFlow = propInfo.EnableFlow,
                    EnabledHex = propInfo.EnableHex,
                    EnabledShield = propInfo.EnableShield,
                    EnabledTower = propInfo.EnableTower,
                    EnabledUI = propInfo.EnableUI
                };

                towerSettings.TowerData = spatialMapItemData;
                //towerSettingsController.SetData(spatialMapItemData);
            }
        }

        public void ShowParticle(bool show)
        {
            if (m_MapSession == null)
            {
                return;
            }

            foreach (var map in m_MapSession.Maps)
            {
                if (map.Controller)
                {
                    map.Controller.ShowPointCloud = show;
                }
            }
        }

        private void CreateSession()
        {
            m_ARSession = Instantiate(m_ARSessionPrefab);
            m_ARSession.gameObject.SetActive(true);
            m_MapFrameFilter = m_ARSession.GetComponentInChildren<SparseSpatialMapWorkerFrameFilter>();
            m_MapSession = new MapSession(m_ARSession, m_MapFrameFilter, m_SelectedMaps);
        }

        private void DestroySession()
        {
            if (m_MapSession != null)
            {
                m_MapSession.Dispose();
                m_MapSession = null;
            }

            if (m_ARSession)
            {
                Destroy(m_ARSession.gameObject);
            }
        }

        #region Firebase RealtimeDb

        public void GetSpatialMaps(
            Action OnPreload,
            Action OnError,
            Action<List<MapMeta>, List<SpatialMapData>> OnSuccess)
        {
            OnPreload?.Invoke();

            RealtimeFirebaseController.GetSpatialMap(new ResultListener<List<SpatialMapData>>()
            {
                OnError = errorMessage =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() => { OnError?.Invoke(); });
                },
                OnSuccess = (spatialMaps, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        m_SelectedMaps.Clear();

                        List<MapMeta> mapMetas = new List<MapMeta>();
                        foreach (var spatialMapData in spatialMaps)
                        {
                            SparseSpatialMapController.SparseSpatialMapInfo spatialMapInfo =
                                new SparseSpatialMapController.SparseSpatialMapInfo();
                            spatialMapInfo.Name = spatialMapData.Name;
                            spatialMapInfo.ID = spatialMapData.Id;

                            List<MapMeta.PropInfo> props = new List<MapMeta.PropInfo>();

                            foreach (var spatialMapItemData in spatialMapData.MapItems)
                            {
                                MapMeta.PropInfo propInfo = new MapMeta.PropInfo();
                                propInfo.Name = spatialMapItemData.Id;

                                propInfo.Position = spatialMapItemData.Position;
                                propInfo.Rotation = spatialMapItemData.Rotation;
                                propInfo.Scale = spatialMapItemData.Scale;

                                propInfo.EnableFlow = spatialMapItemData.EnabledFlow;
                                propInfo.EnableHex = spatialMapItemData.EnabledHex;
                                propInfo.EnableShield = spatialMapItemData.EnabledShield;
                                propInfo.EnableTower = spatialMapItemData.EnabledTower;
                                propInfo.EnableUI = spatialMapItemData.EnabledUI;

                                props.Add(propInfo);
                            }

                            MapMeta mapMeta = new MapMeta(spatialMapInfo, props);
                            mapMetas.Add(mapMeta);
                        }

                        m_SpatialMaps = spatialMaps;
                        OnSuccess?.Invoke(mapMetas, m_SpatialMaps);
                    });
                }
            });
        }

        public void GetSpatialMaps()
        {
            MapMenuView.ShowError(false);
            MapMenuView.ShowLoading(true);

            RealtimeFirebaseController.GetSpatialMap(new ResultListener<List<SpatialMapData>>()
            {
                OnError = errorMessage =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        MapMenuView.ShowLoading(false);
                        MapMenuView.ShowError(true);
                    });
                },
                OnSuccess = (spatialMaps, message) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        m_SelectedMaps.Clear();

                        List<MapMeta> mapMetas = new List<MapMeta>();
                        foreach (var spatialMapData in spatialMaps)
                        {
                            SparseSpatialMapController.SparseSpatialMapInfo spatialMapInfo =
                                new SparseSpatialMapController.SparseSpatialMapInfo();
                            spatialMapInfo.Name = spatialMapData.Name;
                            spatialMapInfo.ID = spatialMapData.Id;

                            List<MapMeta.PropInfo> props = new List<MapMeta.PropInfo>();

                            foreach (var spatialMapItemData in spatialMapData.MapItems)
                            {
                                MapMeta.PropInfo propInfo = new MapMeta.PropInfo();
                                propInfo.Name = spatialMapItemData.Id;

                                propInfo.Position = spatialMapItemData.Position;
                                propInfo.Rotation = spatialMapItemData.Rotation;
                                propInfo.Scale = spatialMapItemData.Scale;

                                propInfo.EnableFlow = spatialMapItemData.EnabledFlow;
                                propInfo.EnableHex = spatialMapItemData.EnabledHex;
                                propInfo.EnableShield = spatialMapItemData.EnabledShield;
                                propInfo.EnableTower = spatialMapItemData.EnabledTower;
                                propInfo.EnableUI = spatialMapItemData.EnabledUI;

                                props.Add(propInfo);
                            }

                            MapMeta mapMeta = new MapMeta(spatialMapInfo, props);
                            mapMetas.Add(mapMeta);
                        }

                        m_SpatialMaps = spatialMaps;

                        MapMenuView.SetData(mapMetas, spatialMaps);
                        MapMenuView.ShowLoading(false);
                        MapMenuView.ShowError(false);
                    });
                }
            });
        }

        public void GetSpatialMaps(ResultListener<List<SpatialMapData>> resultListener)
        {
            RealtimeFirebaseController.GetSpatialMap(resultListener);
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData)
        {
            RealtimeFirebaseController.SaveSpatialMap(spatialMapData, new ResultListener<SpatialMapData>()
            {
                OnSuccess = (spatialMapData, message) => { UnityMainThreadDispatcher.Instance().Enqueue(() => { }); },
                OnError = (errorMessage) => { UnityMainThreadDispatcher.Instance().Enqueue(() => { }); }
            });
        }

        public void SaveSpatialMap(SpatialMapData spatialMapData, ResultListener<SpatialMapData> resultListener)
        {
            RealtimeFirebaseController.SaveSpatialMap(spatialMapData, resultListener);
        }

        public void RemoveSpatialMap(string idSpatialMap, ResultListener<SpatialMapData> resultListener)
        {
            RealtimeFirebaseController.RemoveSpatialMap(idSpatialMap, resultListener);
        }

        #endregion
    }
}