using System;
using System.Collections;
using System.Collections.Generic;
using easyar;
using UnityEngine;

public class FloorController : MonoBehaviour
{
    public GameObject EasyArGameObject;
    public VIOCameraDeviceUnion VioCamera;
    public GameObject FloorPrefab;

    private bool m_IsPlaced = false;
    private GameObject m_FloorGameObject;
    
    private void Start()
    {
        m_IsPlaced = false;
    }

    private void Update()
    {
        if (!m_IsPlaced && VioCamera.isActiveAndEnabled)
        {
            
            var viewPoint = new Vector2(0.5f, 0.333f);
            var points = VioCamera.HitTestAgainstHorizontalPlane(viewPoint);

            Debug.Log($"Floor is Ready. Points: {points.Count}");
            
            if (points.Count > 0)
            {
                m_IsPlaced = true;
                if (m_FloorGameObject == null)
                {
                    m_FloorGameObject = Instantiate(FloorPrefab);
                }

                m_FloorGameObject.transform.position = points[0];

                easyar.GUIPopup.EnqueueMessage("Floor is placed", 5);
            }
        }
    }

    public void Reset()
    {
        m_IsPlaced = false;
    }
    
    public void Reset(VIOCameraDeviceUnion vioCameraDeviceUnion)
    {
        VioCamera = vioCameraDeviceUnion;
        m_IsPlaced = false;
    }
}