﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public bool IsLootAtCamera = false;
    private Transform m_MainCameraTransform;

    private void Start()
    {
        //TODO: fix camera
        //m_MainCameraTransform = GameObject.FindWithTag("ARCamera").transform;
        if (m_MainCameraTransform == null)
        {
            m_MainCameraTransform = Camera.main.transform;
        }
    }

    public void Update()
    {
        if (IsLootAtCamera)
        {
            /*
            Vector3 direction = m_MainCameraTransform.Position - transform.Position;
            Quaternion Rotation = Quaternion.LookRotation(direction);
            Rotation.x = 0;
            Rotation.z = 0;
            transform.Rotation = Rotation;
            */

            Vector3 targetPostition = new Vector3(m_MainCameraTransform.position.x,
                                        transform.position.y,
                                        m_MainCameraTransform.position.z);
            transform.LookAt(targetPostition);
        }
    }
}
