using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Weter
{
    public class SceneManager : MonoBehaviour
    {
        public void LoadScene(string name)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(name);
        }

        public void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }
    }
}
