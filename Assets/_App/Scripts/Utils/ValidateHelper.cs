﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Weter
{
    public class ValidateHelper
    {
        public static void Validate<T>(GameObject gameobject)
        {
            if (gameobject == null || gameobject.GetComponent<T>() == null)
            {
                gameobject = null;
            }
        }
    }
}