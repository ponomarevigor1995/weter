﻿using System;
using UnityEngine;

namespace Weter
{
    public abstract class ExtendedMonoBehaviour : MonoBehaviour
    {
        protected virtual void InitComponents()
        {
        }

        protected virtual void OnValidate()
        {
            InitComponents();
        }
    }
}