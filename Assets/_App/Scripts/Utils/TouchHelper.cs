using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchHelper : MonoBehaviour
{
    public static Vector3? CheckTouch()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0)
                && !EventSystem.current.IsPointerOverGameObject())
            {
                return Input.mousePosition;
            }
        }

        if (Input.touchCount > 0)
        {
            if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)
                && Input.touches[0].phase == TouchPhase.Began)
            {
                return Input.touches[0].position;
            }
        }

        return null;
    }
}
