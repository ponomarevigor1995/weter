using System;
using System.Collections;
using System.Collections.Generic;
using easyar;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class DenseMapSession : MonoBehaviour
    {
        private enum TrackSurfaceMode
        {
            OFF,
            CAMERA_RAY,
            COUNT_SURFACE
        }

        private ARSession m_Session;
        private TouchController m_TouchRoot;
        [SerializeField] private TrackSurfaceMode m_CurrentTrackSurfaceMode = TrackSurfaceMode.COUNT_SURFACE;

        private Color m_MeshColor;
        private Camera m_CameraTarget;
        private DenseSpatialMapBuilderFrameFilter m_DenseMapBuilder;

        [SerializeField] private int m_MinBlockCount = 5;
        [SerializeField] private Vector2 m_ViewPoint = new Vector2(0.5f, 0.333f);
        [SerializeField] private float m_Offset = 0.0f;

        private bool m_IsTracked = false;
        private bool m_IsReadyTrack = false;

        public Action OnTracked;
        public Action<DenseSpatialMapBlockController> m_OnCreateDenseSpatialMapBlock;

        protected virtual void Awake()
        {
            m_CameraTarget = Camera.main;
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
            if (m_IsReadyTrack && !m_IsTracked)
            {
                switch (m_CurrentTrackSurfaceMode)
                {
                    case TrackSurfaceMode.OFF:
                        TrackSurface();
                        break;
                    case TrackSurfaceMode.CAMERA_RAY:
                        TrackSurfaceByCamera();
                        break;
                    case TrackSurfaceMode.COUNT_SURFACE:
                        break;
                }
            }
        }

        public void StartSession(ARSession session,
                                 TouchController touchRoot)
        {
            m_Session = session;
            m_TouchRoot = touchRoot;

            m_DenseMapBuilder = m_Session.GetComponentInChildren<DenseSpatialMapBuilderFrameFilter>();
            m_MeshColor = m_DenseMapBuilder.MeshColor;

            m_IsReadyTrack = true;
            if (m_CurrentTrackSurfaceMode == TrackSurfaceMode.COUNT_SURFACE)
            {
                m_OnCreateDenseSpatialMapBlock = CreateDenseSpatialMapBlock;
                m_DenseMapBuilder.MapCreate += m_OnCreateDenseSpatialMapBlock;
            }
        }

        private void TrackSurface()
        {
            if (m_DenseMapBuilder.MeshBlocks.Count > m_MinBlockCount)
            {
                m_IsTracked = true;
                OnTracked?.Invoke();
            }
        }

        private void TrackSurfaceByCamera()
        {
            Ray ray = m_CameraTarget.ScreenPointToRay(new Vector3(0.5f, 0.333f, 0f));
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                PlaceTouchRoot(hitInfo.point);
                m_IsTracked = true;
                OnTracked?.Invoke();
            }
        }

        private void CreateDenseSpatialMapBlock(DenseSpatialMapBlockController denseSpatialMapBlockController)
        {
            StartCoroutine(MapCreateListenerCoroutine(denseSpatialMapBlockController));
        }

        private IEnumerator MapCreateListenerCoroutine(DenseSpatialMapBlockController denseSpatialMapBlockController)
        {
            if (m_IsTracked)
            {
                yield break;
            }

            Debug.Log($"{name}: MapCreateListenerCoroutine");

            MeshFilter meshFilter = denseSpatialMapBlockController.GetComponent<MeshFilter>();
            int tryCount = 0;
            float delay = 0.25f;

            while (meshFilter.mesh.normals.Length == 0)
            {
                tryCount++;
                if (tryCount > 5)
                {
                    yield break;
                }
                else
                {
                    yield return new WaitForSeconds(delay);
                }
            }

            if (m_IsTracked)
            {
                yield break;
            }

            int sizeVertices = meshFilter.mesh.vertices.Length;
            Vector3 centerPoint = Vector3.zero;
            float pointX = 0f;
            float pointY = 0f;
            float pointZ = 0f;
            for (int i = 0; i < sizeVertices; ++i)
            {
                Vector3 point = meshFilter.mesh.vertices[i];
                pointX += point.x;
                pointY += point.y;
                pointZ += point.z;
            }

            int sizeNormals = meshFilter.mesh.normals.Length;
            Vector3 centerNormal = Vector3.zero;
            float normalX = 0f;
            float normalY = 0f;
            float normalZ = 0f;
            for (int i = 0; i < sizeNormals; ++i)
            {
                Vector3 normal = meshFilter.mesh.normals[i];
                normalX += normal.x;
                normalY += normal.y;
                normalZ += normal.z;
            }

            centerPoint = new Vector3(pointX / sizeVertices, pointY / sizeVertices, pointZ / sizeVertices);
            centerNormal = new Vector3(normalX / sizeNormals, normalY / sizeNormals, normalZ / sizeNormals).normalized;

            if (centerNormal.y > 0.8f)
            {
                Debug.Log($"{name}: MapCreateListenerCoroutine: stop");

                m_IsTracked = true;
                Vector3 worldPosition = denseSpatialMapBlockController.transform.TransformPoint(centerPoint);
                PlaceTouchRoot(worldPosition);
                m_DenseMapBuilder.MapCreate -= m_OnCreateDenseSpatialMapBlock;
                OnTracked?.Invoke();
            }
        }

        private void PlaceTouchRoot(Vector3 worldPosition)
        {
            //worldPosition += ArCamera.transform.forward * m_Offset;

            m_TouchRoot.transform.position = worldPosition;
            Vector3 cameraPosition = m_CameraTarget.transform.position;
            Vector3 targetPosition = new Vector3(cameraPosition.x,
                                                 m_TouchRoot.transform.position.y,
                                                 cameraPosition.z);
            m_TouchRoot.transform.LookAt(targetPosition);
        }

        #region EasyAR Map Settings

        public void ShowMap(bool isShow)
        {
            if (!m_DenseMapBuilder)
            {
                return;
            }

            m_DenseMapBuilder.RenderMesh = isShow;
        }

        public void ShowMap()
        {
            ShowMap(!m_DenseMapBuilder.RenderMesh);
        }

        public void UseCollider(bool useCollider)
        {
            if (!m_DenseMapBuilder)
            {
                return;
            }

            m_DenseMapBuilder.UseCollider = useCollider;
        }

        public void UseCollider()
        {
            UseCollider(!m_DenseMapBuilder.UseCollider);
        }

        public void TransparentMesh(bool isTransparent)
        {
            if (!m_DenseMapBuilder)
            {
                return;
            }

            m_DenseMapBuilder.MeshColor = isTransparent ? Color.clear : m_MeshColor;
        }

        public void TransparentMesh()
        {
            if (m_DenseMapBuilder.MeshColor == Color.clear)
            {
                TransparentMesh(false);
            }
            else
            {
                TransparentMesh(true);
            }
        }

        #endregion
    }
}