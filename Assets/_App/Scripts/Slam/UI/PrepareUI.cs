using System;
using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class PrepareUI : BaseSessionUI
    {
        [SerializeField] private SliderManager m_ScaleSliderManager;
        [SerializeField] private SliderManager m_RotateSliderManager;
        [SerializeField] private Button m_NextButton;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!m_ScaleSliderManager && !m_RotateSliderManager)
            {
                SliderManager[] sliderManagers = GetComponentsInChildren<SliderManager>();
                if (sliderManagers.Length >= 2)
                {
                    m_ScaleSliderManager = sliderManagers[0];
                    m_RotateSliderManager = sliderManagers[1];
                }
            }

            if (!m_NextButton)
            {
                m_NextButton = GetComponentInChildren<Button>();
            }
        }

        private void Awake()
        {
            InitComponents();
     
        }

        private void Start()
        {
            if (m_ScaleSliderManager)
            {
                m_ScaleSliderManager.mainSlider.onValueChanged.AddListener(value => GameSession.Instance.Scale(value));
            }

            if (m_RotateSliderManager)
            {
                m_RotateSliderManager.mainSlider.onValueChanged.AddListener(value => GameSession.Instance.Rotate(value));
            }

            if (m_NextButton)
            {
                m_NextButton.onClick.AddListener(() => GameSession.Instance.SetPlayState());
            }
        }
    }
}
