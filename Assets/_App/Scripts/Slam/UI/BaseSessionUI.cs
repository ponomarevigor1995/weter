using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class BaseSessionUI : ExtendedMonoBehaviour
    {
        [SerializeField] protected SessionUI m_SessionUI;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!m_SessionUI)
            {
                m_SessionUI = GetComponentInParent<SessionUI>();
            }
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}