using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class SessionUI : ExtendedMonoBehaviour, IGameSession
    {
        [SerializeField] private TutorialUI m_TutorialUI;
        [SerializeField] private PrepareUI m_PrepareUI;
        [SerializeField] private PlayUI m_PlayUI;

        private List<BaseSessionUI> m_UIs = new List<BaseSessionUI>();
        
        protected override void InitComponents()
        {
            base.InitComponents();
            BaseSessionUI[] uis = GetComponentsInChildren<BaseSessionUI>();
            if (uis.Length >= 3)
            {
                m_TutorialUI = uis[0] as TutorialUI;
                m_PrepareUI = uis[1] as PrepareUI;
                m_PlayUI = uis[2] as PlayUI;
            }
        }

        private void Awake()
        {
            InitComponents();
            
            m_UIs.Add(m_TutorialUI);
            m_UIs.Add(m_PrepareUI);
            m_UIs.Add(m_PlayUI);
        }

        private void Hide()
        {
            foreach (BaseSessionUI baseSessionUI in m_UIs)
            {
                baseSessionUI.Hide();
            }
        }

        public void SetTutorialState()
        {
            Hide();
            m_TutorialUI.Show();
        }

        public void SetPrepareState()
        {
            Hide();
            m_PrepareUI.Show();
        }

        public void SetPlayState()
        {
            Hide();
            m_PlayUI.Show();
        }

        public void UpdateWind(float value)
        {
            m_PlayUI.UpdateWindSlider(value);
        }
    }
}