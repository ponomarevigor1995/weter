using System;
using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class PlayUI : BaseSessionUI
    {
        [SerializeField] protected SliderManager m_WindSliderManager;
        [SerializeField] protected SliderManager m_LightSliderManager;
        [SerializeField] protected Button m_EnvironmentButton;
        [SerializeField] protected Button m_ShieldButton;

        protected override void InitComponents()
        {
            base.InitComponents();
            
            if (!m_WindSliderManager && !m_LightSliderManager)
            {
                SliderManager[] sliderManagers = GetComponentsInChildren<SliderManager>();
                if (sliderManagers.Length >= 2)
                {
                    m_LightSliderManager = sliderManagers[0];
                    m_WindSliderManager = sliderManagers[1];
                }
            }

            if (!m_EnvironmentButton)
            {
                Transform findButton = transform.Find("EnvironmentButton");
                if (!findButton)
                {
                    m_EnvironmentButton = findButton.GetComponent<Button>();
                }
            }
            
            if (!m_ShieldButton)
            {
                Transform findButton = transform.Find("ShieldButton");
                if (!findButton)
                {
                    m_ShieldButton = findButton.GetComponent<Button>();
                }
            }
        }

        private void Awake()
        {
            InitComponents();
        }

        private void Start()
        {
            if (m_LightSliderManager)
            {
                m_LightSliderManager.mainSlider.onValueChanged.AddListener(value => GameSession.Instance.UpdateLight(value));
            }

            if (m_WindSliderManager)
            {
                m_WindSliderManager.mainSlider.onValueChanged.AddListener(value => GameSession.Instance.UpdateWindSpeed(value));
            }

            if (m_ShieldButton)
            {
                m_ShieldButton.onClick.AddListener(() => GameSession.Instance.SwitchShields());
            }
            
            if (m_EnvironmentButton)
            {
                m_EnvironmentButton.onClick.AddListener(() => GameSession.Instance.SwitchEnvironment());
            }
        }

        public void UpdateWindSlider(float value)
        {
            if (!m_WindSliderManager)
            {
                m_WindSliderManager.mainSlider.value = value;
            }
        }
    }
}