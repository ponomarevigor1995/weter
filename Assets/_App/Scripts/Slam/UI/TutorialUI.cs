using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;

namespace Weter
{
    public class TutorialUI : BaseSessionUI
    {
        [SerializeField] private ModalWindowManager m_ModalWindowInfo;

        protected override void InitComponents()
        {
            base.InitComponents();
        }

        protected void Awake()
        {
            InitComponents();
        }

        public override void Show()
        {
            base.Show();
            if (m_ModalWindowInfo)
            {
                m_ModalWindowInfo.OpenWindow();
            }
        }

        public override void Hide()
        {
            base.Hide();
        }
    }
}