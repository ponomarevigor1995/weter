using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Utils.ColorModels;
using easyar;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class SurfaceGameSession : GameSession
    {
        [Header("EasyAR")]
        
        [SerializeField] public ARSession Session;
        [SerializeField] protected SurfaceTrackerFrameFilter m_Tracker;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!Session)
            {
                Session = FindObjectOfType<ARSession>(true);
            }
            if (Session && !m_Tracker)
            {
                m_Tracker = Session.GetComponentInChildren<SurfaceTrackerFrameFilter>(true);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            if (m_Tracker)
            {
                m_Tracker.Target.TargetFound += () =>
                {
                    SetPrepareState();
                };
            }
        }
    }
}