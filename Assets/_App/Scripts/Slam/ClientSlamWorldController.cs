using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Utils.ColorModels;
using easyar;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class ClientSlamWorldController : MonoBehaviour, IWindObservable, ILightObservable
    {
        [SerializeField] public PreviewPlayUI UiController;

        [SerializeField] public SliderManager WindSliderManager;
        [SerializeField] public bool UseShields = true;

        protected List<IWindObserver> m_WindObservers = new List<IWindObserver>();
        protected List<ILightObserver> m_LightObservers = new List<ILightObserver>();
        protected List<TowerFlow> m_FlowControllers = new List<TowerFlow>();
        protected List<Tower> m_TowerControllers = new List<Tower>();
        protected List<TowerSettings> m_TowerSettingsControllers = new List<TowerSettings>();

        protected void Awake()
        {
        }

        protected void Start()
        {
        }

        protected void Update()
        {
        }

        public void SetActive()
        {
            //m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);

            UpdateWindSpeed(WindSliderManager.mainSlider.value); //
            
            m_FlowControllers.AddRange(FindObjectsOfType<TowerFlow>());
            m_TowerControllers.AddRange(FindObjectsOfType<Tower>());

            /*
            m_FlowControllers.AddRange(FindObjectsOfType<m_TowerFlow>());
            m_TowerControllers.AddRange(FindObjectsOfType<m_Tower>());

            for (int i = 0; i < m_TowerControllers.Count; i++)
            {
                m_FlowControllers[i].UpdateScale(m_TowerControllers[i].transform.localScale.x);
            }

            for (int i = 0; i < m_FlowControllers.Count; i++)
            {
                m_FlowControllers[i].UpdateScale(m_TowerControllers[i].transform.localScale.x);
            }
            */
            
            m_TowerSettingsControllers.AddRange(FindObjectsOfType<TowerSettings>(true));
            
            for (int i = 0; i < m_TowerSettingsControllers.Count; i++)
            {
                m_TowerSettingsControllers[i].ShowSettingsCanvas(false);
            }
        }

        public void Clear()
        {
            m_LightObservers.Clear();
            m_WindObservers.Clear();

            m_TowerControllers.Clear();
            m_FlowControllers.Clear();

            m_TowerSettingsControllers.Clear();

            UiController.Clear();
        }

        public void UpdateWindSpeed(float speed)
        {
            UpdateWindObservers(speed);
        }

        public void EnableShields(bool useShields)
        {
            UseShields = useShields;

            foreach (var flowController in m_FlowControllers)
            {
                flowController.EnableShields(UseShields);
                //TODO
                //m_TowerFlow.UpdateScale(m_TowerRoot.transform.localScale.x);
            }

            foreach (var towerController in m_TowerControllers)
            {
                towerController.EnableShields(UseShields);
            }
        }

        public void UpdateLight(float light)
        {
            UpdateLightObservers(light);
        }

        #region Wind Observer

        public void AddWindObserver(IWindObserver observer)
        {
            m_WindObservers.Add(observer);

            //UiController.EnableShields(true);
            //UiController.EnableWindSlider(true);
        }

        public void RemoveWindObserver(IWindObserver observer)
        {
            m_WindObservers.Remove(observer);
        }

        public void UpdateWindObservers(float speed)
        {
            foreach (var windObserver in m_WindObservers)
            {
                windObserver.UpdateSpeed(speed);
            }
        }

        #endregion

        #region Light Observer

        public void AddLightObserver(ILightObserver observer)
        {
            m_LightObservers.Add(observer);
        }

        public void RemoveLightObserver(ILightObserver observer)
        {
            m_LightObservers.Remove(observer);
        }

        public void UpdateLightObservers(float light)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLight(light);
            }
        }

        public void UpdateScaleLightObservers(float scale)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLightScale(scale);
            }
        }

        #endregion
    }
}