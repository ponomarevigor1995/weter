using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weter;
using Weter.Slam;

namespace Weter
{
    public class WindController : WindObserver
    {
        private List<ParticleSystem> m_WindParticleSystems = new List<ParticleSystem>();

        protected override void Awake()
        {
            base.Awake();
            m_WindParticleSystems.AddRange(GetComponentsInChildren<ParticleSystem>());
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void Update()
        {
            base.Update();
        }

        public override void UpdateSpeed(float speed)
        {
            speed /= 10f;
            base.UpdateSpeed(speed);

            bool isActive = true;
            if (m_Speed <= 0f)
            {
                isActive = false;
            }
            else
            {
                isActive = true;
            }
            
            foreach (var windParticleSystem in m_WindParticleSystems)
            {
                windParticleSystem.gameObject.SetActive(isActive);
                var mainSpeedModule = windParticleSystem.main;
                mainSpeedModule.simulationSpeed = m_Speed;
            }
        }
    }
}