using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Utils.ColorModels;
using easyar;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class SurfaceWorldController : MonoBehaviour, IWindObservable, ILightObservable
    {
        public enum SlamWorldState
        {
            IDLE,
            PREPARE,
            PLAY
        }
        [SerializeField] public ARSession Session;

        [SerializeField] public SessionUI sessionUI;
        [SerializeField] public Tower TowerController;
        [SerializeField] public WindController WindController;
        [SerializeField] public TowerFlow TowerFlowController;

        [SerializeField] public List<GameObject> CityControllers = new List<GameObject>();

        //[SerializeField] public GameObject CityController1;
        //[SerializeField] public GameObject CityController2;

        [SerializeField] public SliderManager WindSliderManager;
        [SerializeField] public bool UseShields = true;

        [SerializeField] public TouchController TouchControl;

        protected Camera m_CameraTarget;
        private SurfaceTrackerFrameFilter m_Tracker;

        private SlamWorldState m_State = SlamWorldState.IDLE;
        protected List<IWindObserver> m_WindObservers = new List<IWindObserver>();
        protected List<ILightObserver> m_LightObservers = new List<ILightObserver>();
        
        private int m_CurrentEnvironment = 0;

        public SlamWorldState State
        {
            get => m_State;
            set => m_State = value;
        }
        
        protected void Awake()
        {
            m_CameraTarget = Camera.main;
            m_Tracker = Session.GetComponentInChildren<SurfaceTrackerFrameFilter>();
            m_Tracker.Target.TargetFound += () =>
            {
                SetPrepareState();
            };
            SetIdleState();

            TouchControl.TurnOn(TouchControl.gameObject.transform, m_CameraTarget, false,
                false, false, false);
            //SwitchEnvironment(0);
        }

        protected void Start()
        {
            TouchControl.OnScale += localScale =>
            {
                TowerFlowController.UpdateScale(localScale.x);
                UpdateScaleLightObservers(localScale.x);
            };
        }

        protected void Update()
        {
            switch (State)
            {
                case SlamWorldState.IDLE:
                    break;
                case SlamWorldState.PREPARE:
                    UpdatePosition();
                    break;
                case SlamWorldState.PLAY:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        public void UpdatePosition()
        {
            Vector3? inputPosition = TouchHelper.CheckTouch();
            if (inputPosition != null)
            {
                Ray ray = m_CameraTarget.ScreenPointToRay((Vector3) inputPosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    TouchControl.transform.position = hitInfo.point;
                    Vector3 targetPostition = new Vector3(m_CameraTarget.transform.position.x,
                        TouchControl.transform.position.y,
                        m_CameraTarget.transform.position.z);
                    TouchControl.transform.LookAt(targetPostition);
                }
            }
        }

        public void SetIdleState()
        {
            State = SlamWorldState.IDLE;
            sessionUI.SetTutorialState();

            TowerController.EnableShields(UseShields);

            TouchControl.TurnOn(
                TouchControl.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                false,
                false);

            if (!Application.isEditor)
            {
                TouchControl.gameObject.SetActive(false);
            }
        }

        public void SetPrepareState()
        {
            State = SlamWorldState.PREPARE;

            sessionUI.SetPrepareState();

            TouchControl.gameObject.SetActive(true);

            TouchControl.TurnOn(
                TouchControl.gameObject.transform,
                m_CameraTarget,
                true,
                true,
                true,
                true);
        }

        public void SetPlayState()
        {
            State = SlamWorldState.PLAY;

            TouchControl.gameObject.SetActive(true);

            sessionUI.SetPlayState();

            /*
            m_TowerRoot.TurnOn(
                m_TowerRoot.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                false,
                false);
            */

            TouchControl.TurnOn(
                TouchControl.gameObject.transform,
                m_CameraTarget,
                false,
                false,
                true,
                true);

            TowerFlowController.EnableShields(UseShields);
            TowerFlowController.UpdateScale(TouchControl.transform.localScale.x);

            UpdateWindSpeed(WindSliderManager.mainSlider.value); //
            UpdateScaleLightObservers(TouchControl.transform.localScale.x);
        }

        public void UpdateWindSpeed(float speed)
        {
            UpdateWindObservers(speed);
        }

        public void EnableShields(bool useShields)
        {
            UseShields = useShields;

            TowerFlowController.EnableShields(UseShields);
            TowerFlowController.UpdateScale(TouchControl.transform.localScale.x);

            TowerController.EnableShields(UseShields);
        }

        public void UpdateLight(float light)
        {
            UpdateLightObservers(light);
        }
        
        public void ShowCity()
        {
            m_CurrentEnvironment++;
            m_CurrentEnvironment %= CityControllers.Count;
            
            ShowCity(m_CurrentEnvironment);
        }

        private void ClearCities()
        {
            foreach (GameObject cityController in CityControllers)
            {
                cityController.gameObject.SetActive(false);
            }
        }
        
        public void ShowCity(int cityPosition)
        {
            ClearCities();
            CityControllers[cityPosition].gameObject.SetActive(true);

            switch (cityPosition)
            {
                case 0:
                    TowerController.transform.localPosition = new Vector3(0f, 0f, 0f);
                    TowerController.ShowHex(true);

                    EnableShields(UseShields);

                    break;
                case 1:
                    TowerController.transform.localPosition = new Vector3(0f, 0f, 0f);
                    TowerController.ShowHex(false);

                    EnableShields(UseShields);
                    UpdateScaleLightObservers(TouchControl.transform.localScale.x);

                    break;
                case 2:

                    TowerController.transform.localPosition = new Vector3(0f, 0.62f, 0f);
                    TowerController.ShowHex(false);

                    EnableShields(UseShields);
                    UpdateScaleLightObservers(TouchControl.transform.localScale.x);

                    break;
            }
        }

        public void Rotate(float rotation)
        {
            TouchControl.transform.localRotation = Quaternion.Euler(0f, rotation, 0f);
        }

        public void Scale(float scale)
        {
            TouchControl.transform.localScale = new Vector3(scale, scale, scale);
            
            TowerFlowController.UpdateScale(TouchControl.transform.localScale.x);
            UpdateScaleLightObservers(TouchControl.transform.localScale.x);
        }

        #region Wind Observer

        public void AddWindObserver(IWindObserver observer)
        {
            m_WindObservers.Add(observer);
        }

        public void RemoveWindObserver(IWindObserver observer)
        {
            m_WindObservers.Remove(observer);
        }

        public void UpdateWindObservers(float speed)
        {
            foreach (var windObserver in m_WindObservers)
            {
                windObserver.UpdateSpeed(speed);
            }
        }

        #endregion

        #region Light Observer

        public void AddLightObserver(ILightObserver observer)
        {
            m_LightObservers.Add(observer);
        }

        public void RemoveLightObserver(ILightObserver observer)
        {
            m_LightObservers.Remove(observer);
        }

        public void UpdateLightObservers(float light)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLight(light);
            }
        }

        public void UpdateScaleLightObservers(float scale)
        {
            foreach (var lightObserver in m_LightObservers)
            {
                lightObserver.UpdateLightScale(scale);
            }
        }

        #endregion
    }
}