using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Utils.ColorModels;
using easyar;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class SlamGameSession : GameSession
    {
        [Header("EasyAR")]

        [SerializeField] public ARSession Session;
        [SerializeField] private DenseMapSession m_DenseMapSession;

        protected override void InitComponents()
        {
            base.InitComponents();
            if (!Session)
            {
                Session = FindObjectOfType<ARSession>(true);
            }

            if (!m_DenseMapSession)
            {
                m_DenseMapSession = FindObjectOfType<DenseMapSession>();
            }
        }
        
        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
            if (m_DenseMapSession)
            {
                m_DenseMapSession.OnTracked = () => { SetPrepareState(); };
                m_DenseMapSession.StartSession(Session, m_TowerRoot);
            }
        }

        public override void SetTutorialState()
        {
            base.SetTutorialState();
            m_DenseMapSession.ShowMap(true);
            m_DenseMapSession.UseCollider(true);
        }

        public override void SetPrepareState()
        {
            base.SetPrepareState();
            m_DenseMapSession.ShowMap(true);
            m_DenseMapSession.UseCollider(true);
        }

        public override void SetPlayState()
        {
            base.SetPlayState();
            m_DenseMapSession.ShowMap(false);
            m_DenseMapSession.UseCollider(false);
        }
    }
}