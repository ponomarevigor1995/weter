using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.UI;

public class PreviewPlayUI : MonoBehaviour
{
    [SerializeField] protected SliderManager m_WindSliderManager;
    [SerializeField] protected Button m_ShieldButton;

    private void Start()
    {
    }

    private void Update()
    {
    }
    
    public void UpdateWindSlider(float value)
    {
        if (!m_WindSliderManager)
        {
            m_WindSliderManager.mainSlider.value = value;
        }
    }

    public void Clear()
    {
        //m_WindSliderManager.gameObject.SetActive(false);
        //m_ShieldButton.gameObject.SetActive(false);
    }
}