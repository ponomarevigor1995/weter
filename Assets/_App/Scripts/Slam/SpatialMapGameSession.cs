using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.Utils.ColorModels;
using easyar;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using Weter;

namespace Weter.Slam
{
    public class SpatialMapGameSession : BaseGameSession
    {
        public static SpatialMapGameSession Instance;

        [SerializeField] private PreviewPlayUI m_PlayUI;

        protected List<TowerFlow> m_FlowControllers = new List<TowerFlow>();
        protected List<Tower> m_TowerControllers = new List<Tower>();
        protected List<TowerSettings> m_TowerSettingsControllers = new List<TowerSettings>();

        protected override void Awake()
        {
            base.Awake();
            Instance = this;
            //m_PlayUI.gameObject.SetActive(false);
        }

        public virtual void Activate()
        {
            m_PlayUI.gameObject.SetActive(true);

            UpdateWindSpeed(m_InitWindValue);
            m_PlayUI.UpdateWindSlider(m_InitWindValue);

            m_FlowControllers.AddRange(FindObjectsOfType<TowerFlow>());
            m_TowerControllers.AddRange(FindObjectsOfType<Tower>());
            m_TowerSettingsControllers.AddRange(FindObjectsOfType<TowerSettings>(true));

            for (int i = 0; i < m_TowerSettingsControllers.Count; i++)
            {
                m_TowerSettingsControllers[i].ShowSettingsCanvas(false);
            }
        }

        public virtual void Clear()
        {
            m_LightObservers.Clear();
            m_WindObservers.Clear();

            m_TowerControllers.Clear();
            m_FlowControllers.Clear();

            m_TowerSettingsControllers.Clear();

            m_PlayUI.Clear();
        }

        public override void EnableShields(bool useShields)
        {
            base.EnableShields(useShields);

            foreach (TowerFlow towerFlow in m_FlowControllers)
            {
                towerFlow.EnableShields(m_UseShields);
            }

            foreach (Tower tower in m_TowerControllers)
            {
                tower.EnableShields(m_UseShields);
            }
        }
    }
}