using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class InfoItemController : MonoBehaviour
    {
        [SerializeField] public InfoData InfoData = new InfoData();
        [HideInInspector] public int Position = 0;

        [SerializeField] private InfoContainerController m_Container;
        [SerializeField] private GameObject m_PanelEffect;
        [SerializeField] private Image m_Icon;
        [SerializeField] private Button m_Button;
        [SerializeField] private Color m_DefaultColor = new Color(1f, 1f, 1f, 1f);
        [SerializeField] private Color m_SelectedColor = new Color(0f, 0.47f, 1f, 1f);
        [SerializeField] private float m_DefaultScale = 1.0f;
        [SerializeField] private float m_SelectedScale = 1.2f;

        [SerializeField] private List<TowerElementController> m_TowerElements = new List<TowerElementController>();

        private InterfaceAnimManager m_InterfaceAnimManager;
        private bool m_IsSelected = false;
        private bool m_IsLookAtCamera = false;

        private void Awake()
        {
            m_InterfaceAnimManager = GetComponent<InterfaceAnimManager>();
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        public void Bind(InfoContainerController infoContainerController, int position,
            Action<InfoItemController> selectAction)
        {
            m_Container = infoContainerController;
            Position = position;
            m_Button.onClick.AddListener(() => { selectAction.Invoke(this); });
        }

        public void Select(bool isSelected)
        {
            m_IsSelected = isSelected;

            if (isSelected)
            {
                m_PanelEffect.transform.localScale = new Vector3(m_SelectedScale, m_SelectedScale, m_SelectedScale);
                m_Icon.color = m_SelectedColor;
            }
            else
            {
                m_PanelEffect.transform.localScale = new Vector3(m_DefaultScale, m_DefaultScale, m_DefaultScale);
                m_Icon.color = m_DefaultColor;
            }
        }

        public void UpdateMaterial(bool isShow)
        {
            foreach (var towerElementController in m_TowerElements)
            {
                towerElementController.UpdateMaterial(isShow);
            }
        }

        public void SetLookAtCamera(bool isLookAtCamera)
        {
            m_IsLookAtCamera = isLookAtCamera;
            if (m_IsLookAtCamera)
            {
                m_PanelEffect.GetComponent<LookAtCamera>().enabled = true;
            }
            else
            {
                m_PanelEffect.GetComponent<LookAtCamera>().enabled = false;
            }
        }
    }
}