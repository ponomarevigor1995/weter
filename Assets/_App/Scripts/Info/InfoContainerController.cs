﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Weter
{
    public class InfoContainerController : MonoBehaviour
    {
        [SerializeField] private DetailInfoController m_DetailInfoController;
        public bool UseLookAtCamera = false;
        
        [SerializeField] private List<InfoItemController> m_InfoItemControllers = new List<InfoItemController>();
        private InfoItemController m_SelectedInfoItemController = null;
        private int m_SelectedPosition = -1;

        private void Awake()
        {
            if (m_InfoItemControllers.Count == 0)
            {
                m_InfoItemControllers.AddRange(GetComponentsInChildren<InfoItemController>());
            }

            for (int i = 0; i < m_InfoItemControllers.Count; i++)
            {
                InfoItemController infoItemController = m_InfoItemControllers[i];
                infoItemController.Bind(this, i, Select);
                infoItemController.SetLookAtCamera(UseLookAtCamera);
            }
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        private void Select(InfoItemController selectedInfoItemController)
        {
            if (m_SelectedInfoItemController != null)
            {
                m_SelectedInfoItemController.Select(false);
                if (m_SelectedInfoItemController == selectedInfoItemController)
                {
                    m_DetailInfoController.Show(false);
                    m_SelectedInfoItemController = null;
                    
                    foreach (var infoItemController in m_InfoItemControllers)
                    {
                        infoItemController.UpdateMaterial(true);
                    }
                    return;
                }
            }

            m_SelectedInfoItemController = selectedInfoItemController;
            m_SelectedInfoItemController.Select(true);
            m_DetailInfoController.SetData(m_SelectedInfoItemController.InfoData);
            m_DetailInfoController.Show(true);
            
            for (int i = 0; i < m_InfoItemControllers.Count; i++)
            {
                InfoItemController infoItemController = m_InfoItemControllers[i];
                bool isShow = false;
                if (i <= m_SelectedInfoItemController.Position)
                {
                    isShow = true;
                }
                else
                {
                    isShow = false;
                }
                infoItemController.UpdateMaterial(isShow);
            }
           
            
            //StartCoroutine(ShowDetailInfo());
        }

        private IEnumerator ShowDetailInfo()
        {
            if (m_DetailInfoController.IsVisible)
            {
                m_DetailInfoController.Show(false);
            }

            yield return new WaitForSeconds(1.5f);

            m_DetailInfoController.SetData(m_SelectedInfoItemController.InfoData);
            m_DetailInfoController.Show(true);
        }
    }
}