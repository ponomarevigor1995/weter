﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Weter
{
    public class DetailInfoController : MonoBehaviour
    {
        [HideInInspector] public bool IsVisible = false;
        
        [SerializeField] private TextMeshProUGUI m_Title;
        [SerializeField] private TextMeshProUGUI m_Description;
        [SerializeField] private Image m_Image;

        private InfoData m_InfoData;
        private InterfaceAnimManager m_InterfaceAnimManager;

        private void Awake()
        {
            m_InterfaceAnimManager = GetComponent<InterfaceAnimManager>();
        }

        public void SetData(InfoData infoData)
        {
            m_InfoData = infoData;
            m_Title.text = m_InfoData.Title;
            m_Description.text = m_InfoData.Description;
            if (infoData.Sprite != null)
            {
                m_Image.sprite = m_InfoData.Sprite;
            }
        }

        public void Show(bool isVisible)
        {
            IsVisible = isVisible;
            if (isVisible)
            {
                m_InterfaceAnimManager.startAppear();
            }
            else
            {
                m_InterfaceAnimManager.startDisappear();
            }
            
            //SetAnimState(CSFHIAnimableState.disappeared);
        }
        
        private void SetAnimState(CSFHIAnimableState state)
        {
            m_InterfaceAnimManager.currentState = state;
            
            foreach (InterfaceAnmElement element in m_InterfaceAnimManager.elementsList)
            {
                element.gameObject.SetActive(true);
                element.enabled = true;
                element.currentState = state;
            }
        }

        private void OnDisable()
        {
            //m_InterfaceAnimManager.startDisappear();
            //IsVisible = false;
        }
    }
    
    [Serializable]
    public class InfoData
    {
        [TextArea(minLines: 2, maxLines: 6)]
        public string Title = "";
        [TextArea(minLines: 6, maxLines: 10)]
        public string Description = "";
        public Sprite Sprite;

        public InfoData()
        {
            
        }
    }
}